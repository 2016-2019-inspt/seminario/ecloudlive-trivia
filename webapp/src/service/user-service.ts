import { getApiUrl } from "@/lib/utils";
import { UserPut } from "@/lib/validation/user";

const USER_URL = getApiUrl() + "/user";

export async function getOrCreateUser(username: string) {
  const response = await fetch(USER_URL + `/${username}`);

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de GET or Create user");
  }

  console.log(response);
  return response.json();
}

export async function updateUser(user: UserPut) {
  const response = await fetch(USER_URL, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    body: JSON.stringify(user),
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de PUT user");
  }

  console.log(response);
  return response.json();
}

export async function postLogin(username: string, password: string) {
  const response = await fetch(USER_URL + "/login", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, password }),
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de POST user login");
  }

  return response.json();
}

export async function postAnswer(
  userId: string,
  answerId: number,
  questionId: number,
  triviaRunId: string
) {

  var userAnserRequest = JSON.stringify({ userId, answerId, questionId, triviaRunId })
  console.log({userAnserRequest})

  const response = await fetch(USER_URL + "/question", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ userId, answerId, questionId, triviaRunId }),
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de POST user answer");
  }

  // return response.json();
}
