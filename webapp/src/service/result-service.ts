import { getApiUrl } from "@/lib/utils";

const RESULTS_API_URL = getApiUrl() + "/results";

export const fetchFinalResults = async (triviaRunId: string) => {
  const response = await fetch(RESULTS_API_URL + `/final/${triviaRunId}`);

  if (!response.ok) {
    throw new Error("Error trayendo los resultados finales");
  }
  return response.json();
};

export const fetchUserResults = async (userId: string) => {
  const response = await fetch(RESULTS_API_URL + `/user/${userId}`);

  if (!response.ok) {
    throw new Error("Error trayendo los resultados del usuario");
  }
  return response.json();
};

export const fetchQuestionResultsByUsers = async (questionId: string) => {
  const response = await fetch(RESULTS_API_URL + `/question/${questionId}`);

  if (!response.ok) {
    throw new Error("Error trayendo los resultados de la pregunta");
  }
  return response.json();
};

export const fetchQuestionResultsByAggregatedUsers = async (
  questionId: string
) => {
  const response = await fetch(RESULTS_API_URL + `/question/${questionId}`);

  if (!response.ok) {
    throw new Error("Error trayendo los resultados agregados de la pregunta");
  }
  return response.json();
};
