import { QuestionPost, QuestionPut } from "@/lib/validation/question";
import { getApiUrl } from "@/lib/utils";
import { Question } from "@/types";

const QUESTION_API_URL = getApiUrl() + "/questions";

export const fetchQuestions = async (triviaId: string) => {
  const response = await fetch(QUESTION_API_URL + `?triviaId=${triviaId}`);

  if (!response.ok) {
    throw new Error("Error trayendo las preguntas");
  }

  const questions: Question[] = await response.json();
  questions.forEach((question) => {
    question.answers.sort((a, b) => {
      return a.correct === b.correct ? 0 : a.correct ? -1 : 1;
    });
  });

  return questions;
};

export const createQuestion = async (question: QuestionPost) => {
  console.log("create question call to api: ", question);

  const body = JSON.stringify(question);
  const response = await fetch(QUESTION_API_URL, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body,
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de POST questions");
  }

  return response.json();
};

export const updateQuestion = async (question: QuestionPut) => {
  const body = JSON.stringify(question);
  console.log({ body });

  const response = await fetch(QUESTION_API_URL, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body,
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de PUT questions");
  }

  return response.json();
};

export const fetchQuestionById = async (id: number) => {
  const response = await fetch(QUESTION_API_URL + `/${id}`);

  if (!response.ok) {
    throw new Error("Error trayendo la pregunta");
  }

  return response.json();
};

export const deleteQuestionById = async (id: number) => {
  const response = await fetch(QUESTION_API_URL + `/${id}`, {
    method: "DELETE",
  });
  if (!response.ok) {
    console.log(response);
    throw new Error("Error de DELETE question by id");
  }

  return response;
};

export const updateQuestionOrder = async (
  orders: { id: number; order: number }[]
) => {
  console.log("updating question order value: ", orders);

  const response = await fetch(QUESTION_API_URL + "/order", {
    method: "PATCH",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(orders),
  });
  if (!response.ok) {
    console.log(response);
    throw new Error("Error de PUT questions order");
  }
};
