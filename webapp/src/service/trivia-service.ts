// Trivia CRUD operations
import { getApiUrl } from "@/lib/utils";
import { TriviaPost, TriviaRunPatch } from "@/lib/validation/trivia";

const TRIVIA_URL = getApiUrl() + "/trivia";
const TRIVIA_RUN_URL = TRIVIA_URL + "/run";

// get trivia
export async function fetchTriviaById(id: string) {
  const response = await fetch(TRIVIA_URL + `/${id}`);

  if (!response.ok) {
    throw new Error("Error de GET trivia by id");
  }

  return response.json();
}

export async function fetchTriviaList() {
  const response = await fetch(TRIVIA_URL);

  if (!response.ok) {
    throw new Error("Error de GET trivia list");
  }

  return response.json();
}

export async function deleteTriviaById(id: string) {
  const response = await fetch(TRIVIA_URL + `/${id}`, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });

  if (!response.ok) {
    throw new Error("Error de DELETE trivia");
  }
}

// modify name, description and countdown. Add linked trivia
export async function upsertTrivia(trivia: TriviaPost) {
  const body = JSON.stringify(trivia);

  const response = await fetch(TRIVIA_URL, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    body,
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de POST trivia");
  }

  return response.json();
}

export async function linkTrivia(from: string, to: string) {
  const queryParams = new URLSearchParams({ from, to });
  const response = await fetch(TRIVIA_URL + "?" + queryParams, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de PUT link trivia");
  }

  return response.json();
}

export async function fetchTriviaRun() {
  const response = await fetch(TRIVIA_RUN_URL);

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de GET trivia run state");
  }

  return response.json();
}

// Also use for reset and next trivia
export async function startTrivia(triviaId: string) {
  const response = await fetch(TRIVIA_RUN_URL + `/start/${triviaId}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de POST trivia start/reset/next");
  }

  return response.json();
}

export async function timeoutTrivia() {
  const response = await fetch(TRIVIA_RUN_URL + `/timeout`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de POST trivia start/reset/next");
  }

  return response.json();
}

export async function showCorrectTrivia() {
  const response = await fetch(TRIVIA_RUN_URL + `/show_correct`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de POST trivia start/reset/next");
  }

  return response.json();
}

export async function nextQuestion() {
  const response = await fetch(TRIVIA_RUN_URL + "/next_question", {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de Next Question");
  }

  return response.json();
}

export async function endTrivia() {
  const response = await fetch(TRIVIA_RUN_URL + "/end", {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de Next Question");
  }
}

export async function updateTriviaRunState(triviaRun: TriviaRunPatch) {
  console.log("Sending to api: ", triviaRun);
  const response = await fetch(TRIVIA_RUN_URL, {
    method: "PATCH",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(triviaRun),
  });

  if (!response.ok) {
    console.log(response);
    throw new Error("Error de PATCH trivia run state");
  }

  return response.json();
}
