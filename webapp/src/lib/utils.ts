import { Answer, Question, User } from "@/types";
import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";
import { getCookie, setCookie } from "cookies-next";
import { getOrCreateUser } from "@/service/user-service";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export function formatDate(input: string | number): string {
  const date = new Date(input);
  return date.toLocaleDateString("en-US", {
    month: "long",
    day: "numeric",
    year: "numeric",
  });
}

export function makeid(length: number) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

export function getApiUrl(): string {
  const apiUrl = process.env.NEXT_PUBLIC_API_URL;
  return apiUrl + "/api";
}

export function shuffleAnswers(question: Question): Answer[] {
  return [...question.answers].sort(() => Math.random() - 0.5);
}

export async function getOrSetUserId(): Promise<User> {
  const triviaUserCookie = getCookie("trivia_user")?.toString();
  if (triviaUserCookie) {
    const splitted = triviaUserCookie.split(":");
    const user: User = { id: splitted[0], username: splitted[1] };
    return new Promise((res, rej) => res(user));
  } else {
    return getOrCreateUser(makeid(12)).then((user: User) => {
      console.log({ user });
      saveUserCookie(user);
      return { id: user.id, username: user.username };
    });
  }
}

export function saveUserCookie(user: User) {
  var nextWeek = new Date();
  nextWeek.setDate(nextWeek.getDate() + 7);
  const usercookievalue = user.id + ":" + user.username;
  setCookie("trivia_user", usercookievalue, { expires: nextWeek });
}
