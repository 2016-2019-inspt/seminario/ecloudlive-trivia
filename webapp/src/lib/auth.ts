import { NextAuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { postLogin } from "@/service/user-service";

export const authOptions: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        username: { label: "Usuario", type: "text", placeholder: "alan" },
        password: {
          label: "Contraseña",
          type: "password",
          placeholder: "turing",
        },
      },
      async authorize(credentials, req) {
        const user = await postLogin(
          credentials.username,
          credentials.password
        );

        if (user) {
          const { password, ...userWoPass } = user;
          return userWoPass;
        } else {
          return null;
        }
      },
    }),
  ],
  // callbacks: {
  //   session: async ({ session, user }) => {
  //     session.userId = user.id;
  //     return session;
  //   },
  // },
};
