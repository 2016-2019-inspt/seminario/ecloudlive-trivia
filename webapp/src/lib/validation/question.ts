import * as z from "zod";

const answerPostSchema = z.object({
  text: z.string(),
  correct: z.boolean(),
  order: z
    .number()
    .nonnegative()
    .or(z.string())
    .pipe(z.coerce.number().int().nonnegative()),
});

export const questionPostSchema = z.object({
  text: z.string(),
  order: z.number().nonnegative(),
  triviaId: z.string(),
  answers: z.array(answerPostSchema).min(2),
});

export type QuestionPost = z.infer<typeof questionPostSchema>;

const answerPutSchema = z.object({
  id: z.number().int().optional(),
  text: z.string(),
  correct: z.boolean(),
  order: z.number().nonnegative(),
});

export const questionPutSchema = z.object({
  id: z.number().int(),
  text: z.string(),
  order: z.number().nonnegative(),
  answers: z.array(answerPutSchema).min(2),
  triviaId: z.string(),
});

export type QuestionPut = z.infer<typeof questionPutSchema>;

export const questionOderPatchSchema = z.array(
  z.object({
    id: z.number().int(),
    order: z.number().int(),
  })
);
