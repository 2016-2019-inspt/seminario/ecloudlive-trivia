import * as z from "zod";

export const triviaPostSchema = z.object({
  id: z.string().optional(),
  name: z.string().min(3).max(128),
  description: z.string().optional(),
  countdown: z
    .number()
    .int()
    .nonnegative()
    .or(z.string())
    .pipe(z.coerce.number().int().nonnegative().min(5)),
  nextTriviaId: z.string().optional(),
  shuffleAnswers: z.boolean(),
});

export type TriviaPost = z.infer<typeof triviaPostSchema>;

export const triviaRunPatchSchema = z.object({
  state: z.string(),
  questionOrder: z.number().int().optional(),
  showAnswer: z.boolean().default(false),
  timeout: z.boolean().default(false),
});

export type TriviaRunPatch = z.infer<typeof triviaRunPatchSchema>;
