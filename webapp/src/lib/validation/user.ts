import * as z from "zod";

const userPutSchema = z.object({
  id: z.string(),
  username: z.string(),
});

export type UserPut = z.infer<typeof userPutSchema>