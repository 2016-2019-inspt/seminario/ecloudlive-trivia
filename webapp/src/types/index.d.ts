export type Answer = {
  id: number;
  text: string;
  correct: boolean;
  questionId?: number;
  order?: number;
};

export type Question = {
  id: number;
  text: string;
  order: number;
  triviaId: string;
  answers: Answer[];
};

export type Trivia = {
  id: string;
  name: string;
  description?: string;
  countdown: number;
  nextTriviaId?: string;
  shuffleAnswers: boolean;
  questionCount: number;
};

export const TRIVIA_RUN_STATE_ENUM = {
  INITIAL = "INITIAL",
  RUNNING = "RUNNING",
  FINAL = "FINAL",
  ENDED = "ENDED",
} as const;

export type TriviaRunStateEnum = keyof typeof TRIVIA_RUN_STATE_ENUM;

export type TriviaRunState = {
  id: string;
  triviaId: string;
  nextTriviaId?: string;
  state: TriviaRunStateEnum;
  currentQuestionId: number;
  currentOrder: number; //required?
  showAnswer: boolean;
  timeout: boolean;
};

export type User = {
  id: string;
  username: string;
  role?: string;
};
