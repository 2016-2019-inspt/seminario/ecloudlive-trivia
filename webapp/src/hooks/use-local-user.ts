import { getOrSetUserId } from "@/lib/utils";
import { useEffect, useState } from "react";

function useLocalUser() {
  const [userId, setUserId] = useState<string>();
  useEffect(() => {
    const fetchUserData = async () => {
      const userData = await getOrSetUserId();
      return userData;
    };
    if (!userId) {
      fetchUserData().then((user) => setUserId(user.id));
    }
  }, [userId]);

  return { userId };
}

export { useLocalUser };
