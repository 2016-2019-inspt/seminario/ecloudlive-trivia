"use client";

import {
  CardTitle,
  CardDescription,
  CardHeader,
  CardContent,
  Card,
} from "@/components/ui/card";
import TriviaCreateDialog from "./trivia-create-dialog";
import TriviaCard from "./trivia-card";
import { Button } from "./ui/button";
import { Icons } from "./icons";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "./ui/alert-dialog";
import { useTriviaListContext } from "@/app/context/trivia-list-context";
import { useRouter } from "next/navigation";

const DeleteButton = ({
  triviaId,
  triviaName,
  handleDeleteTrivia,
}: {
  triviaId: string;
  triviaName: string;
  handleDeleteTrivia: (triviaId: string) => void;
}) => {
  return (
    <AlertDialog>
      <AlertDialogTrigger asChild>
        <Button
          className="px-4 py-2 text-red-500 border-2 border-red-300 hover:bg-red-100 dark:hover:bg-red-800"
          variant="outline"
        >
          <Icons.trash className="w-5 h-5 text-red-500" />
        </Button>
      </AlertDialogTrigger>
      <AlertDialogContent className="text-black">
        <AlertDialogHeader>
          <AlertDialogTitle>
            Esta seguro que quiere borrar la trivia &quot;{triviaName}&quot;?
          </AlertDialogTitle>
          <AlertDialogDescription>
            Se eliminara la trivia permanentemente, esta accion no se podra
            revertir.
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancelar</AlertDialogCancel>
          <AlertDialogAction onClick={() => handleDeleteTrivia(triviaId)}>
            Eliminar
          </AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};

const NewTriviaCard = () => {
  return (
    <Card className="transition duration-200 ease-in-out transform cursor-pointer hover:scale-105">
      <CardHeader>
        <CardTitle>Agregar Nueva</CardTitle>
        <CardDescription>
          Click aqui para crear una nueva trivia
        </CardDescription>
      </CardHeader>
      <CardContent className="flex justify-center items-center">
        <TriviaCreateDialog />
      </CardContent>
    </Card>
  );
};

export default function TriviaCardList() {
  // list of trivias with name, id, description, next-trivia
  const { trivias, handleDeleteTrivia } = useTriviaListContext();
  const router = useRouter();

  return (
    <div className="grid grid-cols-1 gap-8 p-8 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
      <NewTriviaCard />
      {trivias?.map((trivia) => {
        const notRunnable = trivia.questionCount < 1;
        return (
          <TriviaCard
            key={trivia.id}
            id={trivia.id}
            name={trivia.name}
            countdown={trivia.countdown}
            description={trivia.description}
            questionCount={trivia.questionCount}
          >
            <DeleteButton
              triviaId={trivia.id}
              triviaName={trivia.name}
              handleDeleteTrivia={handleDeleteTrivia}
            />
            <Button
              className="px-4 py-2 text-blue-500 border-2 border-blue-300 hover:bg-blue-100 dark:hover:bg-blue-800"
              variant="outline"
              onClick={() => router.push(`/host/trivia/${trivia.id}/edit/`)}
            >
              <Icons.fileEdit className="w-5 h-5 text-blue-500" />
            </Button>

            {/* todo hanlde run check if there are questions, modify trivia object */}
            <Button
              className="px-4 py-2 text-green-500 border-2 border-green-300 hover:bg-green-100 dark:hover:bg-green-800"
              variant="outline"
              disabled={notRunnable}
              onClick={() => {
                router.push(`/host/trivia/${trivia.id}/run/`);
              }}
            >
              <Icons.play className="w-5 h-5 text-green-500" />
            </Button>
          </TriviaCard>
        );
      })}
    </div>
  );
}
