"use client";

import {
  CardTitle,
  CardDescription,
  CardHeader,
  CardContent,
  CardFooter,
  Card,
} from "@/components/ui/card";
import { Question } from "@/types";
import Timer from "./timer";
import { Button } from "./ui/button";
import { Label } from "@radix-ui/react-label";
import { useTriviaRunContext } from "@/app/context/trivia-run-context";

interface HostQuestionCardProps {
  question: Question;
  showCorrect: boolean;
  countdown: number;
  continueHandler: () => void;
  showResultsHandler: () => void;
}

export default function HostQuestionCard({
  showCorrect,
  continueHandler,
  showResultsHandler,
  countdown,
  question,
}: HostQuestionCardProps) {
  const { timeoutStateHandler } = useTriviaRunContext();
  return (
    <Card>
      <CardHeader className="bg-opacity-40 bg-gradient-to-r to-blue-200 backdrop-blur-lg from-slate-200">
        <CardTitle>{question.text}</CardTitle>
        <CardDescription>
          Selecciona una de las siguientes opciones.
        </CardDescription>
        {!showCorrect && (
          <Timer seconds={countdown} timeoutCallback={timeoutStateHandler} />
        )}
      </CardHeader>
      <CardContent>
        <div>
          <h3 className="mt-2 mb-4 text-lg font-semibold" id="answers">
            Opciones
          </h3>
          <ul className="grid grid-cols-1 gap-4">
            {question.answers.map((ans) => {
              return (
                <li
                  className={`flex relative p-4 w-full h-full rounded shadow-lg transition-shadow duration-200 ease-in cursor-pointer hover:ring hover:shadow-xl 
                ${
                  showCorrect && ans.correct
                    ? " ring-2 ring-white ring-opacity-60 ring-offset-2 bg-opacity-100 text-white bg-none ring-offset-green-300 bg-green-600"
                    : "bg-gradient-to-r from-blue-200  to-slate-200"
                }`}
                  key={ans.id}
                >
                  <Label
                    className={` w-full peer h-10 ml-2 text-lg text-left font-medium ${
                      showCorrect && ans.correct
                        ? "text-white"
                        : "text-zinc-800"
                    }`}
                  >
                    {ans.text}
                  </Label>
                </li>
              );
            })}
          </ul>
        </div>
      </CardContent>
      <CardFooter className="flex justify-center items-center bg-gradient-to-r to-blue-200 from-slate-200">
        {showCorrect ? (
          <Button
            variant="outline"
            size="lg"
            className="px-4 py-2 mt-2 text-blue-500 border-2 border-blue-300 hover:bg-blue-800 hover:text-white"
            onClick={continueHandler}
          >
            CONTINUAR
          </Button>
        ) : (
          <Button
            variant="outline"
            size="lg"
            className="px-4 py-2 mt-2 text-blue-500 border-2 border-blue-300 hover:bg-blue-800 hover:text-white"
            onClick={showResultsHandler}
          >
            VER RESULTADOS
          </Button>
        )}
      </CardFooter>
    </Card>
  );
}
