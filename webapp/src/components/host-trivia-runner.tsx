"use client";

import { useTriviaRunContext } from "@/app/context/trivia-run-context";
import { useRouter } from "next/navigation";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "./ui/card";
import { Button } from "./ui/button";
import { Question, Trivia, TriviaRunState } from "@/types";
import HostQuestionCard from "./host-question-card";
import { useEffect } from "react";
import { shuffleAnswers } from "@/lib/utils";
import { fetchFinalResults } from "@/service/result-service";
import useSWR from "swr";

interface HostInitialCardProps {
  trivia: Trivia;
  nextQuestionTriviaHandler: () => void;
}

function HostInitialCard({
  trivia,
  nextQuestionTriviaHandler,
}: HostInitialCardProps) {
  return (
    <Card>
      <CardHeader>
        <CardTitle>{trivia?.name}</CardTitle>
        <CardDescription>{trivia?.description}</CardDescription>
      </CardHeader>
      <CardContent>
        <div className="flex justify-between">
          <p>Cantidad de preguntas: </p> <p>{trivia?.questionCount}</p>
        </div>
        <div className="flex justify-between">
          <p>Tiempo para responder: </p> <p>{trivia?.countdown} segundos</p>
        </div>
      </CardContent>
      <CardFooter>
        <Button onClick={nextQuestionTriviaHandler}>Comenzar</Button>
      </CardFooter>
    </Card>
  );
}

interface HostFinalCardProps {
  trivia: Trivia;
  endTriviaHandler: () => void;
}

function HostFinalCard({ trivia, endTriviaHandler }: HostFinalCardProps) {
  //todo add results
  const { triviaRunState } = useTriviaRunContext();

  // trivia results schema
  // {
  //   "users": 3,
  //   "correct": 3,
  //   "incorrect": 4,
  //   "notAnswered": 2
  // }
  const fetcher = (triviaRunId: string) => {
    return fetchFinalResults(triviaRunId);
  };

  const { data, error, isLoading } = useSWR(triviaRunState.id, fetcher);

  function calculateRate(total: number, part: number): number {
    if (total === 0) {
      return 0;
    }
    return (part / total) * 100;
  }

  return trivia ? (
    <Card>
      <CardHeader>
        <CardTitle>{trivia?.name}</CardTitle>
        <CardDescription>
          La trivia &quot;{trivia?.name}&quot; ha finalizado. Muchas gracias por
          participar
        </CardDescription>
      </CardHeader>
      <CardContent>
        <div className="flex justify-between">
          <p>Cantidad de preguntas: </p> <p>{trivia?.questionCount}</p>
        </div>
        <div className="flex justify-between">
          <p>Total de Participantes: </p> {!isLoading && <p>{data.users} </p>}
        </div>
        <div className="flex justify-between">
          <p>Respuestas correctas: </p>
          {!isLoading && (
            <p>
              {calculateRate(
                data.users * trivia.questionCount,
                data.correct
              ).toFixed(2)}
              {" %"}
            </p>
          )}
        </div>
        <div className="flex justify-between">
          <p>Respuestas incorrectas: </p>
          {!isLoading && (
            <p>
              {calculateRate(
                data.users * trivia.questionCount,
                data.incorrect
              ).toFixed(2)}
              {" %"}
            </p>
          )}
        </div>
        <div className="flex justify-between">
          <p>Sin responder: </p>
          {!isLoading && (
            <p>
              {calculateRate(
                data.users * trivia.questionCount,
                data.notAnswered
              ).toFixed(2)}
              {" %"}
            </p>
          )}
        </div>
      </CardContent>
      <CardFooter>
        <Button onClick={endTriviaHandler}>Finalizar</Button>
      </CardFooter>
    </Card>
  ) : (
    <div>Cargando</div>
  );
}

interface TriviaSwitchStateProps {
  triviaRunState: TriviaRunState;
  trivia: Trivia;
}

function TriviaSwitchState({ triviaRunState, trivia }: TriviaSwitchStateProps) {
  const {
    showResults,
    currentQuestion,
    showAnswerStateHandler,
    nextQuestionStateHandler,
  } = useTriviaRunContext();
  const router = useRouter();

  switch (triviaRunState?.state) {
    case "INITIAL":
      return (
        <HostInitialCard
          nextQuestionTriviaHandler={nextQuestionStateHandler}
          trivia={trivia}
        />
      );
    case "RUNNING":
      return (
        currentQuestion && (
          <HostQuestionCard
            showCorrect={showResults}
            question={currentQuestion}
            countdown={trivia.countdown}
            continueHandler={nextQuestionStateHandler}
            showResultsHandler={showAnswerStateHandler}
          />
        )
      );
    case "FINAL":
      return (
        <HostFinalCard
          trivia={trivia}
          endTriviaHandler={nextQuestionStateHandler}
        />
      );
    case "ENDED":
      // FIXME
      router.push("/host/");
    default:
      null;
    // startTriviaStateHandler();
  }
}

export default function HostTriviaRunner() {
  const { triviaRunState, trivia, startTriviaStateHandler } =
    useTriviaRunContext();

  // get running trivia or start by id
  if (!triviaRunState) {
    startTriviaStateHandler();
  }

  // useEffect(() => {
  //   if (currentQuestion && trivia?.shuffleAnswers) {
  //     shuffleAnswers(currentQuestion);
  //   }
  // }, [trivia, currentQuestion])

  // primero me traigo trivia y preguntas,  y el trivia run recien creado. luego se lo paso al runner runner switch
  // como el switch interactua con el estado de trivia run, y las questions y trivias ya existem, le damos para adelante con la interaccion

  // agregar el timer y el shuffle( shufle podria venir desde el backend)
  if (triviaRunState && trivia) {
    return (
      <TriviaSwitchState triviaRunState={triviaRunState} trivia={trivia} />
    );
  }
}
