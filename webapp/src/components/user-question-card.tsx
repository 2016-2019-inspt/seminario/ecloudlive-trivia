"use client";

import {
  CardTitle,
  CardDescription,
  CardHeader,
  CardContent,
  CardFooter,
  Card,
} from "@/components/ui/card";
import { RadioGroupItem, RadioGroup } from "@/components/ui/radio-group";
import { Question } from "@/types";
import { Button } from "./ui/button";
import { Label } from "./ui/label";

function getUserCardStyleFromState(
  checked: boolean,
  showResult: boolean,
  correct: boolean
): string {
  const isChecked = checked || (showResult && correct);
  const isCorrect = showResult && correct;
  const isIncorrect = showResult && !correct;
  const isActive = !showResult && checked;
  const commonStyles =
    "block flex relative p-4 w-full h-full bg-gradient-to-r from-blue-200 rounded shadow-lg transition-shadow duration-200 ease-in cursor-pointer hover:ring hover:shadow-xl to-slate-200 ";
  const activeStyles = "ring-offset-sky-300 bg-sky-800";
  const correctStyles = "ring-offset-green-300 bg-green-600";
  const incorrectStyles = "ring-offset-red-300 bg-red-600";
  const defaultStyles = "";

  return `
    ${
      isChecked
        ? "ring-2 ring-white ring-opacity-60 ring-offset-2 bg-opacity-75 text-white bg-none"
        : defaultStyles
    }
    ${isCorrect ? correctStyles : ""}
    ${isIncorrect ? incorrectStyles : ""}
    ${isActive ? activeStyles : ""}
    ${commonStyles}
  `;
}

interface UserQuestionCardProps {
  question: Question;
  showCorrect: boolean;
  countdown: number;
  confirmed: boolean;
  selected?: string;
  timeout: boolean;
  selectionHandler: (ansId: string) => void;
  confirmHandler: () => void;
}

export default function UserQuestionCard({
  question,
  showCorrect,
  countdown,
  confirmed,
  selected,
  timeout,
  confirmHandler,
  selectionHandler,
}: UserQuestionCardProps) {
  return (
    <Card>
      <CardHeader className="bg-opacity-40 bg-gradient-to-r to-blue-200 backdrop-blur-lg from-slate-200">
        <CardTitle>{question.text}</CardTitle>
        <CardDescription>
          Selecciona una de las siguientes opciones.
        </CardDescription>
        {/* {showCorrect ? (
          <div className="flex justify-center p-4 text-lg font-semibold" ></div>
        ) : (
          <Timer seconds={countdown} timeoutCallback={() => {}} />
        )} */}
      </CardHeader>
      <CardContent>
        <RadioGroup
          aria-labelledby="answers"
          value={selected}
          onValueChange={selectionHandler}
          disabled={confirmed || timeout}
        >
          <h3 className="mt-2 mb-4 text-lg font-semibold" id="answers">
            Opciones
          </h3>
          <div className="grid grid-cols-1 gap-4">
            {question.answers.map((ans) => {
              const ansId: string = String(ans.id);
              const isChecked: boolean = selected === ansId;
              return (
                <div
                  key={ans.id}
                  className={getUserCardStyleFromState(
                    isChecked,
                    showCorrect,
                    ans.correct
                  )}
                >
                  <Label
                    htmlFor={ansId}
                    className={` w-full peer h-10 ml-2 text-lg text-left font-medium ${
                      isChecked || (showCorrect && ans.correct)
                        ? "text-white"
                        : "text-zinc-800"
                    }`}
                  >
                    {ans.text}

                    <RadioGroupItem
                      className="hidden peer"
                      id={ansId}
                      value={ansId}
                    />
                  </Label>
                </div>
              );
            })}
          </div>
        </RadioGroup>
      </CardContent>
      <CardFooter className="flex justify-center items-center bg-gradient-to-r to-blue-200 from-slate-200">
        <Button
          variant="outline"
          size="lg"
          className="px-4 py-2 mt-2 text-blue-500 border-2 border-blue-300 hover:bg-blue-800 hover:text-white"
          onClick={confirmHandler}
          disabled={confirmed || timeout}
        >
          CONFIRMAR
        </Button>
      </CardFooter>
    </Card>
  );
}
