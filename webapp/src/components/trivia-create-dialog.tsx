"use client";

import { useState } from "react";
import { Icons } from "./icons";
import TriviaForm from "./trivia-form";
import { Button } from "./ui/button";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "./ui/dialog";
import { useTriviaListContext } from "@/app/context/trivia-list-context";

export default function TriviaCreateDialog() {
  const [open, setOpen] = useState(false);
  const { handleSaveTrivia } = useTriviaListContext();
  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <Button
          className="py-2 px-4 border-2 border-blue-300 text-blue-500 hover:bg-blue-100 dark:hover:bg-blue-800"
          variant="outline"
        >
          <Icons.plus className="w-5 h-5 text-blue-500" />
        </Button>
      </DialogTrigger>
      <DialogContent className="text-black sm:max-w-[425px]">
        <DialogHeader className="text-black">
          <DialogTitle className="">Nueva Trivia</DialogTitle>
          <DialogDescription>
            Agrega los datos requeridos para configurar la trivia. Luego hace
            click en guardar cuando hayas finalizado o en cancelar para volver
            sin guardar.
          </DialogDescription>
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <TriviaForm
            formId="trivia_form"
            onSubmitCallback={() => setOpen(false)}
            handleSaveTrivia={handleSaveTrivia}
          />
        </div>

        <DialogFooter className="grid grid-cols-2 gap-2">
          <DialogClose asChild>
            <Button
              type="button"
              className="py-2 px-4 border-2 border-red-300 text-red-500 hover:bg-red-100 dark:hover:bg-red-800"
              variant="outline"
            >
              Cancelar
            </Button>
          </DialogClose>

          <Button
            type="submit"
            className="py-2 px-4 border-2 border-green-300 text-green-500 hover:bg-green-100 dark:hover:bg-green-800"
            variant="outline"
            form="trivia_form"
          >
            Guardar
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}
