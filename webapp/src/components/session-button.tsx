"use client";

import { signIn, signOut, useSession } from "next-auth/react";
import { Button } from "./ui/button";

export default function SessionButton() {
  const { data: session } = useSession();

  if (session && session.user) {
    return (
      <Button
        variant="link"
        className="text-lg text-blue-500 hover:text-blue-700"
        onClick={() => signOut()}
      >
        Logout
      </Button>
    );
  } else {
    return (
      <Button
        variant="link"
        className="text-lg text-blue-500 hover:text-blue-700"
        onClick={() => signIn()}
      >
        Login
      </Button>
    );
  }
}
