import { Trivia } from "@/types";
import TriviaForm from "./trivia-form";
import { Button } from "./ui/button";
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from "./ui/card";
import { TriviaPost } from "@/lib/validation/trivia";
import { upsertTrivia } from "@/service/trivia-service";
import { useRouter } from "next/navigation";
import { useSWRConfig } from "swr";

interface TriviaInfoEditCardProps {
  trivia: Trivia;
  closeEditTrivia: () => void;
}

export default function TriviaInfoEditCard({
  trivia,
  closeEditTrivia,
}: TriviaInfoEditCardProps) {
  const { mutate } = useSWRConfig();

  const router = useRouter();
  // todo move to context when add question count field
  const handleSaveTrivia = async (trivia: TriviaPost) => {
    const newTrivia: Trivia = await upsertTrivia(trivia);
    mutate("trivia", newTrivia);
    router.refresh();
  };
  return (
    <Card className="h-full">
      <CardHeader>
        <CardTitle>Editando: {trivia.name}</CardTitle>
      </CardHeader>
      <CardContent>
        <p className="text-sm">
          Edite las configuraciones necesarias, presione Guardar para guardar
          los cambios, o Cancelar para descartarlos.
        </p>
        <div className="flex flex-col space-y-4 mt-4">
          <div>
            <div className="grid gap-4 py-4">
              <TriviaForm
                formId="trivia_form"
                trivia={trivia}
                triviaId={trivia.id}
                onSubmitCallback={closeEditTrivia}
                handleSaveTrivia={handleSaveTrivia}
              />
            </div>
          </div>
        </div>
      </CardContent>
      <CardFooter className="grid grid-cols-2 gap-2">
        <Button
          type="button"
          className="py-2 px-4 border-2 border-red-300 text-red-500 hover:bg-red-100 dark:hover:bg-red-800"
          variant="outline"
          onClick={closeEditTrivia}
        >
          Cancelar
        </Button>
        <Button
          type="submit"
          className="py-2 px-4 border-2 border-green-300 text-green-500 hover:bg-green-100 dark:hover:bg-green-800"
          variant="outline"
          form="trivia_form"
        >
          Guardar
        </Button>
      </CardFooter>
    </Card>
  );
}
