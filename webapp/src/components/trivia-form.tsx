"use client";

import { Input } from "./ui/input";
import { SubmitHandler, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { TriviaPost, triviaPostSchema } from "@/lib/validation/trivia";
import { Switch } from "./ui/switch";
import { Form, FormControl, FormField, FormItem, FormLabel } from "./ui/form";
import { toast } from "../hooks/use-toast";
import { Trivia } from "@/types";


interface TriviaFormProps {
  formId: string;
  triviaId?: string;
  trivia?: Trivia;
  onSubmitCallback?: () => void;
  handleSaveTrivia: (trivia: TriviaPost) => void;
}

export default function TriviaForm({
  formId,
  triviaId,
  trivia,
  onSubmitCallback = () => {},
  handleSaveTrivia
}: TriviaFormProps) {
  
  const triviaHookForm = useForm<TriviaPost>({
    resolver: zodResolver(triviaPostSchema),
    defaultValues: {
      name: trivia?.name ?? "",
      description: trivia?.description ? trivia.description : "",
      countdown: trivia?.countdown ?? 0,
      shuffleAnswers: trivia?.shuffleAnswers ?? true,
    },
  });
  

  const onSubmit: SubmitHandler<TriviaPost> = async (data: TriviaPost) => {
    // handle create vs update trivia
    if (triviaId) {
      data.id = triviaId;
    }

    try {
      handleSaveTrivia(data);
      onSubmitCallback();
      toast({
        title: "Se han guardado los cambios",
        description: `se guardaron los cambios para la trivia ${data.name}.`,
      });
    } catch (e) {
      console.log(e);
      toast({
        variant: "destructive",
        title: "Error guardando los cambios",
        description: `La trivia ${data.name} no pudo guardarse correctamente deebido a un error.`,
      });
    }
  };

  return (
    <Form {...triviaHookForm}>
      <form
        id={formId}
        onSubmit={triviaHookForm.handleSubmit(onSubmit)}
        className="grid gap-4 py-4"
      >
        <FormField
          control={triviaHookForm.control}
          name="name"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">Nombre</FormLabel>
              <FormControl>
                <Input
                  id="name"
                  className="border-2 border-blue-300 text-blue-500 col-span-3"
                  placeholder="Trivia 1"
                  {...field}
                />
              </FormControl>
            </FormItem>
          )}
        />
        <FormField
          control={triviaHookForm.control}
          name="description"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">Description</FormLabel>
              <FormControl>
                <Input
                  id="description"
                  className="border-2 border-blue-300 text-blue-500 col-span-3"
                  placeholder="Descripcion de Trivia 1"
                  {...field}
                />
              </FormControl>
            </FormItem>
          )}
        />
        <FormField
          control={triviaHookForm.control}
          name="countdown"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">Tiempo por pregunta</FormLabel>
              <FormControl>
                <Input
                  id="countdown"
                  className="border-2 border-blue-300 text-blue-500"
                  placeholder="10 s"
                  type="number"
                  {...field}
                />
              </FormControl>
            </FormItem>
          )}
        />
        <FormField
          control={triviaHookForm.control}
          name="shuffleAnswers"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">Barajar opciones</FormLabel>
              <FormControl>
                <Switch
                  id="shuffle-answers"
                  defaultChecked
                  checked={field.value}
                  onCheckedChange={field.onChange}
                />
              </FormControl>
            </FormItem>
          )}
        />
      </form>
    </Form>
  );
}
