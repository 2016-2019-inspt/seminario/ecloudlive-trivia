import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "./ui/card";

interface TriviaCardProps {
  id: string;
  name: string;
  description?: string;
  countdown: number;
  questionCount: number;
  children: React.ReactNode;
  // boolean has next
  // trivia object ???
  // algunas estadisticas de runs anteriores?
  // count de preguntas ?
}

export default function TriviaCard({
  name,
  description,
  countdown,
  questionCount,
  children,
}: TriviaCardProps) {
  return (
    <Card className="cursor-pointer transition duration-200 ease-in-out transform hover:scale-105">
      <CardHeader>
        <CardTitle>{name}</CardTitle>
        <CardDescription>{description}</CardDescription>
      </CardHeader>
      <CardContent>
        <p className="text-sm">{questionCount} preguntas</p>
        {questionCount > 0 ? (
          <p className="text-sm">{`${
            questionCount * countdown
          } segundos duracion total`}</p>
        ) : null}
        <div className="flex justify-end space-x-2 mt-4">{children}</div>
      </CardContent>
    </Card>
  );
}
