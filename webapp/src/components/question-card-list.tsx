"use client";

import { useQuestionListContext } from "@/app/context/question-list-context";
import { Icons } from "./icons";
import QuestionCreateEditDialog from "./question-create-dialog";
import { Button } from "./ui/button";
import { Card, CardContent, CardHeader, CardTitle } from "./ui/card";

const NewQuestionCard = () => {
  return (
    <Card className="transition duration-200 ease-in-out transform hover:ring">
      <CardContent className="items-center p-4 flex flex-row space-x-4 ">
        <QuestionCreateEditDialog />
        <div className="flex-none">
          <CardTitle className="text-lg">Nueva Pregunta</CardTitle>
          <p className="text-sm text-zinc-500 dark:text-zinc-400">
            Haga click para crear una nueva pregunta
          </p>
        </div>
      </CardContent>
    </Card>
  );
};

export default function QuestionCardList() {
  const { questions, handleDeleteQuestion } = useQuestionListContext();

  return (
    <div className="col-span-1 sm:col-span-2 md:col-span-2 lg:col-span-3">
      <Card className="h-full">
        <CardHeader>
          <CardTitle>Preguntas</CardTitle>
        </CardHeader>
        <CardContent>
          <div className="flex flex-col space-y-4">
            <NewQuestionCard />
            {questions?.map((question, index) => (
              <Card
                key={index}
                className="transition duration-200 ease-in-out transform hover:ring"
              >
                <CardContent className="items-center p-4 flex flex-row justify-between space-x-4 ">
                    <div className="flex-none">
                      <CardTitle className="text-lg">{question.text}</CardTitle>
                      <p className="text-sm text-zinc-500 dark:text-zinc-400">
                        {question.answers.length} opciones
                      </p>
                    </div>

                    <div className="flex justify-end space-x-2 mt-4">
                      <QuestionCreateEditDialog isEdit questionIndex={index} />
                      <Button
                        className="py-2 px-4 border-2 border-red-300 text-red-500 hover:bg-red-100 dark:hover:bg-red-800"
                        variant="outline"
                        onClick={() => handleDeleteQuestion(question.id)}
                      >
                        <Icons.trash className="w-5 h-5 text-red-500" />
                      </Button>
                    </div>
                </CardContent>
              </Card>
            ))}
          </div>
        </CardContent>
      </Card>
    </div>
  );
}
