"use client";

import TriviaInfoCard from "./trivia-info-card";
import TriviaInfoEditCard from "./trivia-info-edit-card";
import QuestionCardList from "./question-card-list";
import { QuestionListContextProvider, useQuestionListContext } from "@/app/context/question-list-context";

function TriviaInfoEdit() {

  const {trivia, editTrivia, switchEditTrivia} = useQuestionListContext();

  return (
    <>
      {trivia ? (
        <>
          {editTrivia ? (
            <TriviaInfoEditCard
              trivia={trivia}
              closeEditTrivia={switchEditTrivia}
            />
          ) : (
            <TriviaInfoCard
              trivia={trivia}
              onEditCallback={switchEditTrivia}
            />
          )}
        </>
      ) : null}
    </>
  );
}

//nombre, duracion, timer, shuffle answer order
//dropdown para siguiente trivia -> validacion en backend para la creacion
export default function TriviaQuestionsEdit({
  triviaId,
}: {
  triviaId: string;
}) {
  return (
    <QuestionListContextProvider triviaId={triviaId}>
      <div className="relative min-h-screen bg-gradient-to-br from-blue-200 via-blue-400 to-blue-600">
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-8 p-8">
          {/* todo: add loading skeleton while waiting on trivia info and question list */}
          <TriviaInfoEdit />
          <QuestionCardList />
        </div>
      </div>
    </QuestionListContextProvider>
  );
}
