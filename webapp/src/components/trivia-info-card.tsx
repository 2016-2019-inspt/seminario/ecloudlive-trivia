import { Trivia } from "@/types";
import { Icons } from "./icons";
import { Button } from "./ui/button";
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from "./ui/card";
import { Label } from "./ui/label";
import { Separator } from "./ui/separator";

interface TriviaInfoCardInterface {
  onEditCallback: () => void;
  trivia: Trivia;
}

export default function TriviaInfoCard({
  trivia,
  onEditCallback,
}: TriviaInfoCardInterface) {
  return (
    <Card className="h-full">
      <CardHeader>
        <CardTitle>Trivia: {trivia.name}</CardTitle>
      </CardHeader>
      <CardContent>
        <p className="text-sm">
          Configuraciones de la trivia, presiona editar para realizar cambios.
        </p>
        <div className="flex flex-col space-y-4 mt-4">
          {trivia.description && (
            <>
              <Separator className="my-4" />
              <div className="space-y-1">
                <Label htmlFor="description">Descripcion</Label>
                <p className="text-right" id="description">
                  {trivia.description}
                </p>
              </div>
            </>
          )}

          <Separator className="my-4" />
          <div className="space-y-1">
            <Label htmlFor="countdown">Tiempo por pregunta</Label>
            <p className="text-right" id="countdown">
              {trivia.countdown} segundos
            </p>
          </div>
          <Separator className="my-4" />
          <div className="space-y-1">
            <Label htmlFor="shuffleAnswers">Barajar opciones?</Label>
            <p className="text-right" id="shuffleAnswers">
              {trivia.shuffleAnswers ? "si" : "no"}
            </p>
          </div>
          <Separator className="my-4" />
        </div>
      </CardContent>
      <CardFooter>
        <Button
          className="py-2 px-4 border-2 border-blue-300 text-blue-500 hover:bg-blue-100 dark:hover:bg-blue-800"
          variant="outline"
          onClick={onEditCallback}
        >
          <Icons.fileEdit className="w-5 h-5 text-blue-500" />
          Editar
        </Button>
      </CardFooter>
    </Card>
  );
}
