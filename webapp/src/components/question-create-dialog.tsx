"use client";

import { useState } from "react";
import { Icons } from "./icons";
import { Button } from "./ui/button";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "./ui/dialog";
import QuestionForm from "./question-form";
import { useQuestionListContext } from "@/app/context/question-list-context";

export default function QuestionCreateEditDialog({
  isEdit = false,
  questionIndex
}: {
  isEdit?: boolean;
  questionIndex?: number;
}) {
  const [open, setOpen] = useState(false);
  const { triviaId, questions, handleSaveQuestion } = useQuestionListContext();
  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        {isEdit ? (
          <Button
            className="py-2 px-4 border-2 border-blue-300 text-blue-500 hover:bg-blue-100 dark:hover:bg-blue-800"
            variant="outline"
          >
            <Icons.fileEdit className="w-5 h-5 text-blue-500" />
          </Button>
        ) : (
          <Button
            className="py-2 px-4 border-2 border-blue-300 text-blue-500 hover:bg-blue-100 dark:hover:bg-blue-800"
            variant="outline"
          >
            <Icons.plus className="w-5 h-5 text-blue-500" />
          </Button>
        )}
      </DialogTrigger>
      <DialogContent className="text-black sm:max-w-[425px]">
        <DialogHeader className="text-black">
          {isEdit ? (
            <>
              <DialogTitle>Editar Pregunta</DialogTitle>
              <DialogDescription>
                Edita la pregunta y sus opciones de respuesta. Cuando finalices,
                presiona en Guardar o Cancelar para descartar los cambios.
              </DialogDescription>
            </>
          ) : (
            <>
              <DialogTitle>Nueva Pregunta</DialogTitle>
              <DialogDescription>
                Agrega la pregunta, luego como minimo crea una respuesta
                correcta y una respuesta incorrecta. Puedes agreagar mas
                respuestas incorrectas tambien. Presiona en Guardar cuando hayas
                terminado o Cancelar para no guardar los cambios.
              </DialogDescription>
            </>
          )}
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <QuestionForm
            formId="question_form"
            triviaId={triviaId}
            onSubmitCallback={() => setOpen(false)}
            handleSaveQuestion={handleSaveQuestion}
            order={questions.length}
            question={isEdit ? questions[questionIndex] : null}
          />
        </div>

        <DialogFooter className="grid grid-cols-2 gap-2">
          <DialogClose asChild>
            <Button
              type="button"
              className="py-2 px-4 border-2 border-red-300 text-red-500 hover:bg-red-100 dark:hover:bg-red-800"
              variant="outline"
            >
              Cancelar
            </Button>
          </DialogClose>

          <Button
            type="submit"
            className="py-2 px-4 border-2 border-green-300 text-green-500 hover:bg-green-100 dark:hover:bg-green-800"
            variant="outline"
            form="question_form"
          >
            Guardar
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}
