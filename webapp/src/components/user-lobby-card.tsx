import Hamster from "./hamster/hamster";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "./ui/card";

interface UserLobbyCardProps {
  triviaName?: string;
  description?: string;
}

export default function UserLobbyCard({
  triviaName,
  description,
}: UserLobbyCardProps) {
  return (
    <>
      <Card className="flex flex-col w-full h-full grow md:h-auto">
        <CardHeader className="bg-opacity-40 bg-gradient-to-r to-blue-200 backdrop-blur-lg from-slate-200 grow-0">
          <CardTitle>
            {triviaName ? triviaName : "Bienvenido a Trivia ECLOUD"}
          </CardTitle>
          <CardDescription>
            {description
              ? description
              : "La trivia ya esta por comenzar. Esta pantalla se actualizara cuando el presentador de inicio a la trivia."}
          </CardDescription>
        </CardHeader>
        <CardContent className="flex flex-col justify-center items-center p-6 flex1 grow">
          <Hamster />
          <p className="mt-8 text-lg">Espere unos instantes...</p>
          <p className="text-lg">Esta pantalla se actualizara al iniciar</p>
        </CardContent>
        <CardFooter className="flex justify-end bg-gradient-to-r to-blue-200 from-slate-200 grow-0"></CardFooter>
      </Card>
    </>
  );
}
