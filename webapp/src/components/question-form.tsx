"use client";

import {
  QuestionPost,
  QuestionPut,
  questionPostSchema,
  questionPutSchema,
} from "@/lib/validation/question";
import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useFieldArray, useForm } from "react-hook-form";
import { Form, FormControl, FormField, FormItem, FormLabel } from "./ui/form";
import { Input } from "./ui/input";
import { Separator } from "./ui/separator";
import { Label } from "./ui/label";
import { Button } from "./ui/button";
import { Icons } from "./icons";
import { Question } from "@/types";
import { useEffect, useState } from "react";

interface QuestionFormProps {
  formId: string;
  triviaId: string;
  question?: Question;
  order: number;
  onSubmitCallback?: () => void;
  handleSaveQuestion: (question: QuestionPost) => void;
}

export default function QuestionForm({
  formId,
  triviaId,
  question,
  order,
  onSubmitCallback = () => {},
  handleSaveQuestion,
}: QuestionFormProps) {
  const conditionalResolver = () => {
    const isPutSchema = question && "id" in question;
    const schema = isPutSchema ? questionPutSchema : questionPostSchema;
    return zodResolver(schema);
  };

  const questionHookForm = useForm<QuestionPost | QuestionPut>({
    resolver: conditionalResolver(),
    mode: "onChange", // what does this does?
    defaultValues: question ?? {
      triviaId: triviaId,
      order: order,
      answers: [
        { correct: true, order: 0 },
        { correct: false, order: 1 },
      ],
    },
  });
  const {
    fields: answerFields,
    append,
    remove,
  } = useFieldArray({
    control: questionHookForm.control,
    name: "answers",
  });
  const { setValue } = questionHookForm;
  const onSubmit: SubmitHandler<QuestionPut | QuestionPost> = async (
    data: QuestionPut | QuestionPost
  ) => {
    // console.log("Entra al onSubmit -------------------------------------------------------")
    // console.log({ data });
    handleSaveQuestion(data);
    onSubmitCallback();
    // if ("id" in data) {
    //   // Handle PutSchema submission
    //   console.log("PutSchema submitted:", data);
    // } else {
    //   // Handle PostSchema submission
    //   console.log("PostSchema submitted:", data);
    // }
  };

  useEffect(() => {
    answerFields.forEach((field, index) => {
      setValue(`answers.${index}.order`, index);
    });
  }, [answerFields, setValue]);

  // console.log(questionHookForm.formState.defaultValues);
  // console.log(questionHookForm.formState.isValid);
  // console.log(questionHookForm.watch());
  // console.log(questionHookForm.formState.errors);
  

  return (
    <Form {...questionHookForm}>
      <form
        id={formId}
        onSubmit={questionHookForm.handleSubmit(onSubmit)}
        className="grid gap-4 py-2"
      >
        {question?.id && (
          <FormField
            name="id"
            control={questionHookForm.control}
            defaultValue={question.id}
            render={({ field }) => (
              <FormItem hidden>
                <FormControl>
                  <Input {...field} readOnly />
                </FormControl>
              </FormItem>
            )}
          />
        )}
        <FormField
          control={questionHookForm.control}
          name="text"
          render={({ field }) => {
            return (
              <FormItem className="grid">
                <FormLabel className="">Pregunta</FormLabel>
                <FormControl>
                  <Input
                    id="text"
                    className="col-span-3 text-blue-700 border-2 border-blue-300"
                    placeholder="Cual es la pregunta?"
                    {...field}
                  />
                </FormControl>
              </FormItem>
            );
          }}
        />
        <Separator />
        <div className="grid grid-cols-8 gap-2">
          <Label className="col-span-7 text-center">Opciones</Label>
          <Button
            className="grid px-4 py-2 text-blue-500 border-2 border-blue-300 hover:bg-blue-100 dark:hover:bg-blue-800"
            variant="outline"
            type="button"
            onClick={() =>
              append({ text: "", correct: false, order: answerFields.length })
            }
          >
            <Icons.plus className="w-5 h-5 text-blue-500" />
          </Button>

          <ul className="col-span-8">
            <li key={0} className="grid grid-cols-8 my-1">
              <FormField
                control={questionHookForm.control}
                name="answers.0.text"
                render={({ field }) => {
                  return (
                    <FormItem className="col-span-7">
                      <FormControl>
                        <Input
                          id="answers.0.text"
                          className="text-blue-700 border-2 border-green-200"
                          placeholder="Respuesta correcta"
                          {...field}
                        />
                      </FormControl>
                    </FormItem>
                  );
                }}
              />
            </li>
            <li key={1} className="grid grid-cols-8 my-1">
              <FormField
                control={questionHookForm.control}
                name="answers.1.text"
                render={({ field }) => {
                  return (
                    <FormItem className="col-span-7">
                      <FormControl>
                        <Input
                          id="answers.1.text"
                          className="text-blue-700 border-2 border-red-200"
                          placeholder="Respuesta incorrecta"
                          {...field}
                        />
                      </FormControl>
                    </FormItem>
                  );
                }}
              />
            </li>

            {answerFields.map((field, index) => {
              if (index == 0 || index == 1) return null;

              return (
                <li key={field.id} className="grid grid-cols-8 gap-2 my-1">
                  <FormField
                    control={questionHookForm.control}
                    name={`answers.${index}.text`}
                    render={({ field }) => {
                      return (
                        <FormItem className="col-span-7">
                          <FormControl>
                            <Input
                              id={`answers.${index}.text`}
                              className="text-blue-700 border-2 border-red-200"
                              placeholder="Respuesta incorrecta"
                              {...field}
                            />
                          </FormControl>
                        </FormItem>
                      );
                    }}
                  />
                  <FormField
                    key={field.id}
                    control={questionHookForm.control}
                    name={`answers.${index}.order`}
                    render={({ field }) => (
                      <FormItem hidden>
                        <FormControl>
                          <Input {...field} readOnly />
                        </FormControl>
                      </FormItem>
                    )}
                  />

                  <Button
                    className="grid col-span-1 px-4 py-2 text-red-500 border-2 border-red-300 hover:bg-red-100 dark:hover:bg-red-800"
                    variant="outline"
                    type="button"
                    onClick={() => remove(index)}
                  >
                    <Icons.minus className="w-5 h-5 text-red-500" />
                  </Button>
                </li>
              );
            })}
          </ul>
        </div>
      </form>
    </Form>
  );
}
