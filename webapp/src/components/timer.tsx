"use client";

import { cn } from "@/lib/utils";
import { useEffect, useState } from "react";

interface TimerProps {
  className?: string;
  seconds: number;
  timeoutCallback: () => void;
}

export default function Timer({
  className,
  seconds,
  timeoutCallback,
}: TimerProps) {
  const [timeLeft, setTimeLeft] = useState(seconds);
  useEffect(() => {
    if (!timeLeft) {
      timeoutCallback();
      return;
    }

    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);
    return () => clearInterval(intervalId);
  }, [timeLeft, timeoutCallback]);
  return (
    <span
      className={cn("flex justify-center p-4 text-lg font-semibold", className)}
    >
      {timeLeft}
    </span>
  );
}
