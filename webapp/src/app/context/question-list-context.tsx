"use client";

import { QuestionPost, QuestionPut } from "@/lib/validation/question";
import {
  createQuestion,
  deleteQuestionById,
  fetchQuestions,
  updateQuestion,
  updateQuestionOrder,
} from "@/service/question-service";
import { fetchTriviaById } from "@/service/trivia-service";
import { Question, Trivia } from "@/types";
import { useRouter } from "next/navigation";
import { createContext, useContext, useState } from "react";
import useSWR from "swr";

interface QuestionListContextInterface {
  questions: Question[] | [];
  trivia?: Trivia;
  triviaId: string;
  editTrivia: boolean;
  switchEditTrivia: () => void;
  handleSaveQuestion: (question: QuestionPost) => void;
  handleDeleteQuestion: (questionId: number) => void;
}

export const QuestionListContext = createContext<QuestionListContextInterface>({
  questions: [],
  editTrivia: false,
  triviaId: "",
  switchEditTrivia: () => {},
  handleSaveQuestion: ({}) => {},
  handleDeleteQuestion: ({}) => {},
});

export const useQuestionListContext = () => {
  const context = useContext(QuestionListContext);

  if (context == undefined) {
    throw new Error(
      "useQuestionListContext must be used within a QuestionListContextProvider"
    );
  }
  return context;
};

export function QuestionListContextProvider({
  triviaId,
  children,
}: {
  triviaId: string;
  children: React.ReactNode;
}) {
  const router = useRouter();

  // Trivia methods
  const { data: trivia, error: triviaerror } = useSWR<Trivia>("trivia", () =>
    fetchTriviaById(triviaId)
  );
  if (triviaerror) {
    console.log(triviaerror);
    router.push("/host");
  }
  const [editTrivia, setEditTrivia] = useState<boolean>(false);
  const switchEditTrivia = () => {
    setEditTrivia(!editTrivia);
  };

  // Question methods
  const {
    data: questions = [],
    mutate,
    error,
  } = useSWR<Question[]>("questions", () => fetchQuestions(triviaId));

  const handleSaveQuestion = async (question: QuestionPost | QuestionPut) => {
    let newQuestion: Question;
    if ("id" in question) {
      newQuestion = await updateQuestion(question);
    } else {
      newQuestion = await createQuestion(question);
    }
    mutate([...questions, newQuestion]);
    router.refresh();
  };
  const handleDeleteQuestion = async (questionId: number) => {
    await deleteQuestionById(questionId);
    const newQuestionList = questions.filter(
      (question) => question.id != questionId
    );
    refreshQuestionOrderValue(newQuestionList);
  };
  const refreshQuestionOrderValue = async (qList: Question[] = questions) => {
    const orderUpdateElements: { id: number; order: number }[] = [];
    for (let i = 0; i < qList.length; i++) {
      const q = qList[i];
      q.order = i;

      const order = { id: q.id, order: i };
      orderUpdateElements.push(order);
      await updateQuestionOrder(orderUpdateElements);
    }
    mutate([...qList]);
    router.refresh();
  };

  return (
    <QuestionListContext.Provider
      value={{
        trivia,
        triviaId,
        editTrivia,
        questions,
        switchEditTrivia,
        handleDeleteQuestion,
        handleSaveQuestion,
      }}
    >
      {children}
    </QuestionListContext.Provider>
  );
}
