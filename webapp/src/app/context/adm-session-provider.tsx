"use client";

import React, { ReactNode } from "react";
import { SessionProvider } from "next-auth/react";

interface Props {
  children: ReactNode;
}

function AdminSessionProvider({ children }: Props) {
  return <SessionProvider>{children}</SessionProvider>;
}

export default AdminSessionProvider;
