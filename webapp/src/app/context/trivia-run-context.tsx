"use client";

import { shuffleAnswers } from "@/lib/utils";
import { fetchQuestionById, fetchQuestions } from "@/service/question-service";
import {
  endTrivia,
  fetchTriviaById,
  fetchTriviaRun,
  nextQuestion,
  showCorrectTrivia,
  startTrivia,
  timeoutTrivia,
  updateTriviaRunState,
} from "@/service/trivia-service";
import { Question, Trivia, TriviaRunState } from "@/types";
import { ReactNode, createContext, useContext, useState } from "react";
import useSWRImmutable from "swr/immutable";

interface TriviaRunContextInterface {
  trivia: Trivia;
  currentQuestion: Question;
  triviaRunState: TriviaRunState;
  showResults: boolean;
  isTimeout: boolean;
  startTriviaStateHandler: () => void;
  nextQuestionStateHandler: () => void;
  showAnswerStateHandler: () => void;
  timeoutStateHandler: () => void;
  endTriviaStateHandler: () => void;
}

export const TriviaRunContext = createContext<
  TriviaRunContextInterface | undefined
>(undefined);

export const useTriviaRunContext = () => {
  const context = useContext(TriviaRunContext);

  if (context == undefined) {
    throw new Error(
      "useTriviaRunContext must be used within a TriviaRunContextProvider"
    );
  }
  return context;
};

export function TriviaRunContextProvider({
  triviaId,
  children,
}: {
  triviaId: string;
  children: ReactNode;
}) {
  const {
    data: trivia,
    error: tError,
    isLoading: loadingTrivia,
  } = useSWRImmutable<Trivia>("trivia", () => fetchTriviaById(triviaId));
  const {
    data: questions = [],
    error: qError,
    isLoading: loadingQuestions,
  } = useSWRImmutable<Question[]>("questions", () => fetchQuestions(triviaId));
  // const { data: triviaRunState, isLoading: loadingRun } =
  // useSWR<TriviaRunState>("run-state", fetchTriviaRun, {
  //   refreshInterval: 1500,
  // });
  const [currentQuestion, setCurrentQuestion] = useState<Question>();
  const [triviaRunState, setTriviaRunState] = useState<TriviaRunState>();
  const [showResults, setShowResults] = useState<boolean>(false);
  const [isTimeout, setIsTimeout] = useState<boolean>(false);

  const startTriviaStateHandler = async () => {
    if (!triviaRunState) {
      const startTriviaState: TriviaRunState = await startTrivia(triviaId);
      setTriviaRunState(startTriviaState);
      const question: Question = questions.find(
        (q) => q.id === startTriviaState.currentQuestionId
      );

      if (trivia && trivia.shuffleAnswers && question) {
        question.answers = shuffleAnswers(question);
      }

      setCurrentQuestion(question);
      console.log("start", triviaRunState);
      console.log({ currentQuestion });
      console.log({ trivia });
    }
  };

  const nextQuestionStateHandler = async () => {
    const nextQuestionState: TriviaRunState = await nextQuestion();
    setShowResults(nextQuestionState.showAnswer);
    setIsTimeout(nextQuestionState.timeout);
    if (nextQuestionState.state === "RUNNING") {
      const question: Question = questions.find(
        (q) => q.id === nextQuestionState.currentQuestionId
      );

      if (trivia && trivia.shuffleAnswers) {
        question.answers = shuffleAnswers(question);
      }

      setCurrentQuestion(question);
    }
    setTriviaRunState(nextQuestionState);
  };

  const showAnswerStateHandler = async () => {
    const showAnswerState = await showCorrectTrivia();
    setShowResults(true);
    setIsTimeout(false);
    setTriviaRunState({ ...showAnswerState });
  };

  const timeoutStateHandler = async () => {
    const timeoutState = await timeoutTrivia();
    setIsTimeout(true);
    setTriviaRunState(timeoutState);
  };

  //meant for cancelling a trivia
  const endTriviaStateHandler = async () => {
    await endTrivia();
  };

  // if (!loadingQuestions && !loadingTrivia) return null;

  return (
    <TriviaRunContext.Provider
      value={{
        trivia,
        currentQuestion,
        triviaRunState,
        showResults,
        isTimeout,
        startTriviaStateHandler,
        nextQuestionStateHandler,
        showAnswerStateHandler,
        timeoutStateHandler,
        endTriviaStateHandler,
      }}
    >
      {children}
    </TriviaRunContext.Provider>
  );
}
