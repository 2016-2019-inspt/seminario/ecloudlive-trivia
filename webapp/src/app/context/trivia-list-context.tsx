"use client";

import { TriviaPost } from "@/lib/validation/trivia";
import {
  deleteTriviaById,
  fetchTriviaList,
  upsertTrivia,
} from "@/service/trivia-service";
import { Trivia } from "@/types";
import { useRouter } from "next/navigation";
import { createContext, useContext } from "react";
import useSWR from "swr";

interface TriviaListCtxInterface {
  trivias: Trivia[] | [];
  handleSaveTrivia: (trivia: TriviaPost) => void;
  handleDeleteTrivia: (triviaId: string) => void;
}

export const TriviaListContext = createContext<TriviaListCtxInterface>({
  trivias: [],
  handleSaveTrivia: () => {},
  handleDeleteTrivia: () => {},
});

export const useTriviaListContext = () => {
  const context = useContext(TriviaListContext);

  if (context == undefined) {
    throw new Error(
      "useTriviaListContext must be used within a TriviaListContextProvider"
    );
  }
  return context;
};

export function TriviaListContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const router = useRouter();
  const { data: trivias = [], mutate } = useSWR<Trivia[]>("trivias", () =>
    fetchTriviaList()
  );

  const handleSaveTrivia = async (trivia: TriviaPost) => {
    const newTrivia: Trivia = await upsertTrivia(trivia);
    mutate([...trivias, newTrivia]);
    router.refresh();
  };

  const handleDeleteTrivia = async (triviaId: string) => {
    const res = await deleteTriviaById(triviaId);
    const newList = trivias.filter((trivia) => trivia.id != triviaId);
    mutate([...newList]);
    router.refresh();
  };

  return (
    <TriviaListContext.Provider
      value={{ trivias, handleSaveTrivia, handleDeleteTrivia }}
    >
      {children}
    </TriviaListContext.Provider>
  );
}
