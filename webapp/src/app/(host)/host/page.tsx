"use client"

import { TriviaListContextProvider } from "@/app/context/trivia-list-context";
import TriviaCardList from "@/components/trivia-card-list";
import { useSession } from "next-auth/react";

export default function HostMainPage() {

  // if it is running move to corresponding /run
  const { data: session } = useSession();

  if (session && session.user) {
    return (
      <TriviaListContextProvider>
        <TriviaCardList />
      </TriviaListContextProvider>
    );
  }
}
