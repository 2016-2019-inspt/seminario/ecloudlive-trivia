import TriviaQuestionsEdit from "@/components/trivia-questions-edit";


interface TriviaQuestionsEditProps {
  params: { triviaId: string };
}

export default function TriviaQuestionsEditPage({
  params,
}: TriviaQuestionsEditProps) {
  // pasar como parametro la trivia id, envolver en el questions context
  return (
    <>
      <TriviaQuestionsEdit triviaId={params.triviaId} />
    </>
  );
}
