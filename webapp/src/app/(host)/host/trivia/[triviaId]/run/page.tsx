import { TriviaRunContextProvider } from "@/app/context/trivia-run-context";
import HostTriviaRunner from "@/components/host-trivia-runner";

interface TriviaRunProps {
  params: { triviaId: string };
}

export default function TriviaRunPage({ params }: TriviaRunProps) {
  return (
    <>
      <div className="w-full flex justify-center mt-6">
        <div className="w-96">
          <TriviaRunContextProvider triviaId={params.triviaId}>
            <HostTriviaRunner />
          </TriviaRunContextProvider>
        </div>
      </div>
    </>
  );
}
