"use client";

import AdminSessionProvider from "@/app/context/adm-session-provider";
import SessionButton from "@/components/session-button";
import { Button } from "@/components/ui/button";
import { useRouter } from "next/navigation";

interface HostLayoutProps {
  children: React.ReactNode;
}

export default function HostLayout({ children }: HostLayoutProps) {
  const router = useRouter();
  return (
    <AdminSessionProvider>
      <nav className="flex justify-between items-center p-6 bg-white bg-opacity-100">
        <div className="text-2xl font-bold text-blue-500">ECLOUD Trivia</div>
        <div className="flex space-x-4">
          <Button
            variant="link"
            className="text-lg text-blue-500 hover:text-blue-700"
            onClick={() => router.push("/host")}
          >
            Inicio
          </Button>
          <SessionButton/>
        </div>
      </nav>
      {children}
    </AdminSessionProvider>
  );
}
