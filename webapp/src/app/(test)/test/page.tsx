"use client";

"use client";

import HostQuestionCard from "@/components/host-question-card";
import UserQuestionCard from "@/components/user-question-card";
import { useState } from "react";

const mockQuestion = {
  id: 1,
  text: "cual es el rio mas corto del mundo?",
  order: 0,
  triviaId: "c442717e-7029-4f18-be7b-138f4834c443",
  answers: [
    {
      id: 3,
      text: "Amazonas",
      correct: false,
      questionId: 1,
      order: 3,
    },
    {
      id: 2,
      text: "Nilo",
      correct: false,
      questionId: 1,
      order: 2,
    },
    {
      id: 4,
      text: "Correntoso",
      correct: false,
      questionId: 1,
      order: 0,
    },
    {
      id: 1,
      text: "de La Plata",
      correct: true,
      questionId: 1,
      order: 1,
    },
  ],
};

export default function TestPage() {
  const [selected, setSelected] = useState<string>();
  const [confirmed, setConfirmed] = useState<boolean>();

  const confirmHandler = async () => {
    console.log("confirm clicked");
    // if (selected) {
    //   setConfirmed(true);
    //   postAnswer(userId, parseInt(selected));
    // }
  };

  const selectionHandler = async (ansId: string) => {
    console.log("selection handler call " + ansId);
    setSelected(ansId);
    // const ans: Answer = currentQuestion.answers.find(ans => String(ans.id) === selected)
    // console.log("confirmed answer: ", ans);
    // !confirmed && setSelected(String(ans.id));
  };
  {
    /* <UserQuestionCard
      question={mockQuestion}
      showCorrect={false}
      countdown={10}
      confirmed={false}
      confirmHandler={confirmHandler}
      selectionHandler={selectionHandler}
      selected={selected}
      /> */
  }

  return (
    <>
      {/* <HostQuestionCard
        showCorrect={true}
        continueHandler={() => {}}
        showResultsHandler={() => {}}
        countdown={10}
        question={mockQuestion}
      /> */}
    </>
  );
}
