"use client";

import { Question, Trivia, TriviaRunState } from "@/types";
import { useEffect, useState } from "react";
import useSWR from "swr";
import { postAnswer } from "@/service/user-service";
import { fetchTriviaById, fetchTriviaRun } from "@/service/trivia-service";
import { fetchQuestionById } from "@/service/question-service";
import UserLobbyCard from "@/components/user-lobby-card";
import UserQuestionCard from "@/components/user-question-card";
import { getOrSetUserId, shuffleAnswers } from "@/lib/utils";

export default function UserTriviaPage() {
  const [isRunning, setIsRunning] = useState<boolean>(false);
  const [showCorrect, setShowCorrect] = useState<boolean>(false);
  const [confirmed, setConfirmed] = useState<boolean>(false);
  // const { data: questions, isLoading: loadingQuestions } = useSWRImmutable(
  //   "uquestions",
  //   fetchQuestion
  // );

  // const { userId } = useLocalUser();

  const [userId, setUserId] = useState<string>();
  useEffect(() => {
    const fetchUserData = async () => {
      const userData = await getOrSetUserId();
      return userData;
    };
    if (!userId) {
      fetchUserData().then((user) => setUserId(user.id));
    }
  }, [userId]);

  const [currentQuestion, setCurrentQuestion] = useState<Question>();
  const [selected, setSelected] = useState<string>();
  const [timer, setTimerValue] = useState<number>();
  const [shuffle, setShuffle] = useState<boolean>();
  const { data: triviaRunState, isLoading: loadingRun } =
    useSWR<TriviaRunState>("run-state", fetchTriviaRun, {
      refreshInterval: 1500,
    });

  useEffect(() => {
    if (triviaRunState?.state === "RUNNING") {
      setIsRunning(true);
    } else {
      setIsRunning(false);
    }
  }, [triviaRunState]);

  useEffect(() => {
    if (isRunning && !triviaRunState.showAnswer) {
      setConfirmed(false);
      fetchTriviaById(triviaRunState.triviaId).then((trivia: Trivia) => {
        setShuffle(trivia.shuffleAnswers);
        setTimerValue(trivia.countdown);
      });
      fetchQuestionById(triviaRunState.currentQuestionId).then(
        (q: Question) => {
          if (shuffle) {
            q.answers = shuffleAnswers(q);
          }
          setCurrentQuestion(q);
        }
      );
    }
    setShowCorrect(triviaRunState?.showAnswer);
  }, [triviaRunState, isRunning, shuffle]);

  useEffect(() => {
    if (triviaRunState?.timeout && !confirmed && selected) {
      postAnswer(
        userId,
        parseInt(selected),
        currentQuestion.id,
        triviaRunState.id
      );
    }
  }, [
    triviaRunState?.timeout,
    confirmed,
    currentQuestion?.id,
    selected,
    triviaRunState?.id,
    userId,
  ]);

  const confirmHandler = async () => {
    if (selected) {
      setConfirmed(true);
      postAnswer(
        userId,
        parseInt(selected),
        currentQuestion.id,
        triviaRunState.id
      );
    }
  };

  const selectionHandler = async (ansId: string) => {
    // const ans: Answer = currentQuestion.answers.find(
    //   (ans) => ansId === selected
    // );
    console.log("confirmed answer: ", ansId);
    !confirmed && setSelected(ansId);
  };

  return (
    <div className="flex justify-center items-center mx-auto w-full h-screen md:w-96 md:h-auto">
      <div className="justify-center content-center p-4 my-6 h-full grow md:h-auto">
        {isRunning && currentQuestion && timer ? (
          <UserQuestionCard
            question={currentQuestion}
            showCorrect={showCorrect}
            countdown={timer}
            confirmed={confirmed}
            selected={selected}
            timeout={triviaRunState.timeout}
            confirmHandler={confirmHandler}
            selectionHandler={selectionHandler}
          />
        ) : (
          <UserLobbyCard />
        )}
      </div>
    </div>
  );
}
