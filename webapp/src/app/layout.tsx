import "./globals.css";
import { Encode_Sans, Roboto } from "next/font/google";

interface RootLayoutProps {
  children: React.ReactNode;
}

export default function RootLayout({ children }: RootLayoutProps) {
  return (
    <html lang="es">
      <head />
      <body className="w-full text-white bg-instit-cel">
        <div className="relative min-h-screen bg-gradient-to-br from-blue-200 via-blue-400 to-blue-600">
          {/* <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 w-3/4 h-3/4 bg-white rounded-full opacity-20" />
          <div className="absolute top-1/4 left-1/4 transform -translate-x-1/2 -translate-y-1/2 w-1/2 h-1/2 bg-white rounded-full opacity-20" />
          <div className="absolute bottom-1/4 right-1/4 transform translate-x-1/2 translate-y-1/2 w-1/3 h-1/3 bg-white rounded-full opacity-20" /> */}
          {children}
        </div>
      </body>
    </html>
  );
}
