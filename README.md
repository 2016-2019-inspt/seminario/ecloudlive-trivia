# ecloudlive-trivia

## Development environment

---
### Backend

#### Prerequisites

1. Java 17
2. Gradle 7.6 + (gradle wrapper is provided)
3. Docker & docker compose
4. Bash (if you are on windows, try either a bash docker image, the WLS or a virtual machine)

#### Build

```shell
sh ./scripts/build.sh api
```

#### Run
```shell
sh ./scripts/run.sh be-dev
cd ./backend/api
./gradlew bootRun
```

---
### Frontend

#### Prerequisites

1. Node 19+
2. Docker & docker compose

#### Build
Run after making changes to the webapp in order to check for possible errors before building the docker image.

```shell
sh ./scripts/build.sh webapp
```

#### Run
```shell
sh ./scripts/run.sh fe-dev
yarn dev
```

---
### Tools

#### PG Admin
web based GUI for postgres database management
1. visit `http://localhost:5050/` and provide the following credentials

```
email:    admin@admin.com
password: root
```

2. click Add new server, then fill in General tab with the name you like 
and fill hostname address with the service name in docker-compose.yml: `db`

Documentation links provided in the dashboard main page.

- Alternatively run docker exec to run psql:
```shell
# docker exec -it  <container-name> psql -U <dataBaseUserName> <dataBaseName>
  docker exec -it ecloud-trivia-dev-db-1 psql -U admin trivia
  
trivia=# SELECT * FROM trivia;
     id     |   name    | description | countdown | updated_at 
------------+-----------+-------------+-----------+------------
 trivia_uid | el nombre |             |        12 | 
(1 row)

trivia=# exit
```

#### Redis Insights
web based GUI for redis management
1. visit `http://localhost:8001` and check only the first slider for accepting the EULA.
2. fill the host with `redis`, port with `6379`

- Troubleshooting
  - if the docker container fails to start with a message like "volume permission denied" run this:
  ```shell
   sudo chown -R 1001 data-dev/redisinsight
  ```

- As an alternative to Redisinsights GUI, use the redis CLI inside the container. 
All redis cli commands are documented in Redis official [website](https://redis.io/commands/) 
```shell
docker exec -it ecloud-trivia-redis-1 redis-cli

127.0.0.1:6379> keys *
1) "\xac\xed\x00\x05t\x00\nrun_status"
2) "run_status"
```

