-- CreateTable
CREATE TABLE IF NOT EXISTS "users" (
                         "id" TEXT NOT NULL,
                         "username" TEXT NOT NULL,
                         "password" TEXT,
                         "role" TEXT DEFAULT 'USER',
                         "created_at" TIMESTAMP DEFAULT now(),
                         CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE IF NOT EXISTS "trivia" (
                          "id" TEXT NOT NULL DEFAULT 'trivia_uid',
                          "name" TEXT NOT NULL,
                          "description" TEXT,
                          "countdown" INTEGER NOT NULL,
                          "next_trivia_id" TEXT,
                          "shuffle_answers" BOOLEAN DEFAULT TRUE,
                          "updated_at" TIMESTAMP DEFAULT now(),
                          CONSTRAINT "trivia_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE IF NOT EXISTS "questions" (
                             "id" SERIAL NOT NULL,
                             "text" TEXT NOT NULL,
                             "order" INTEGER NOT NULL,
                             "trivia_id" TEXT NOT NULL,
                             CONSTRAINT "question_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE IF NOT EXISTS "answers" (
                           "id" SERIAL NOT NULL,
                           "text" TEXT NOT NULL,
                           "correct" BOOLEAN NOT NULL DEFAULT false,
                           "question_id" INTEGER NOT NULL,
                           "order" INTEGER NOT NULL,
                           CONSTRAINT "answer_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE IF NOT EXISTS "user_answer" (
                               "id" SERIAL NOT NULL,
                               "user_id" TEXT NOT NULL,
                               "answer_id" INTEGER NOT NULL,
                               "question_id" INTEGER NOT NULL,
                               "created_at" TIMESTAMP DEFAULT now(),
                               CONSTRAINT "user_answer_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE IF NOT EXISTS "trivia_run_state" (
                                     "id" TEXT NOT NULL DEFAULT 'trivia_run_state',
                                     "trivia_id" TEXT NOT NULL,
                                     "next_trivia_id" TEXT,
                                     "state" TEXT NOT NULL DEFAULT 'INITIAL',
                                     "current_question_id" INTEGER,
                                     "current_order" INTEGER,
                                     "show_answer" BOOLEAN NOT NULL DEFAULT false,
                                     "timeout" BOOLEAN NOT NULL DEFAULT false,
                                     "updated_at" TIMESTAMP DEFAULT now(),
                                     CONSTRAINT "trivia_run_status_pkey" PRIMARY KEY ("id")
);


CREATE UNIQUE INDEX "user_username_key" ON "users"("username");

CREATE UNIQUE INDEX "user_answer_user_id_answer_id_key" ON "user_answer"("user_id", "answer_id");

CREATE UNIQUE INDEX "user_answer_user_id_question_id_key" ON "user_answer"("user_id", "question_id");

CREATE UNIQUE INDEX "question_trivia_id_order_key" ON "questions"("trivia_id", "order");


ALTER TABLE "answers" ADD CONSTRAINT "answer_question_id_fkey" FOREIGN KEY ("question_id") REFERENCES "questions"("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "questions" ADD CONSTRAINT "question_trivia_id_fkey" FOREIGN KEY ("trivia_id") REFERENCES "trivia"("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "user_answer" ADD CONSTRAINT "user_answer_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE "user_answer" ADD CONSTRAINT "user_answer_answer_id_fkey" FOREIGN KEY ("answer_id") REFERENCES "answers"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE "user_answer" ADD CONSTRAINT "user_answer_question_id_fkey" FOREIGN KEY ("question_id") REFERENCES "questions"("id") ON DELETE RESTRICT ON UPDATE CASCADE;


CREATE VIEW trivia_results_aggregate AS
SELECT
    users_count,
    question_count,
    correct_answers,
    incorrect_answers,
    (question_count * users_count) - answer_count AS unanswered_questions
FROM
    (SELECT COUNT(DISTINCT id) AS users_count
     FROM users
     WHERE users.role = 'USER') uc
        CROSS JOIN
    (SELECT COUNT(ua.id) AS correct_answers
     FROM user_answer ua
              LEFT JOIN answers a on ua.answer_id = a.id
     WHERE a.correct = true) cc
        CROSS JOIN
    (SELECT COUNT(ua.id) AS incorrect_answers
     FROM user_answer ua
              LEFT JOIN answers a on ua.answer_id = a.id
     WHERE correct = false) ic
        CROSS JOIN
    (SELECT COUNT(id) AS question_count FROM questions) qc
        CROSS JOIN
    (SELECT COUNT(id) AS answer_count FROM user_answer) ac;

CREATE VIEW user_answers_question AS
SELECT
    users.id AS user_id,
    questions.text AS question,
    correct_answer.text AS correct,
    ua_text.text AS answer
FROM
    users
        CROSS JOIN
    questions
        LEFT JOIN
    answers AS correct_answer ON questions.id = correct_answer.question_id AND correct_answer.correct = TRUE
        LEFT JOIN
    user_answer ON user_answer.user_id = users.id AND questions.id = user_answer.question_id
        LEFT JOIN
    answers AS ua_text ON user_answer.answer_id = ua_text.id
WHERE
        users.role = 'USER'
ORDER BY
    users.id,
    questions.id;

CREATE VIEW question_results AS
SELECT
    q.id AS question_id,
    q.text AS question,
    a.id AS answer_id,
    a.text AS answer,
    a.correct AS correct,
    u.id AS user_id,
    u.username AS "user"
FROM
    questions q
        LEFT JOIN
    answers a ON q.id = a.question_id
        LEFT JOIN
    user_answer ua ON ua.answer_id = a.id
        LEFT JOIN
    users u on ua.user_id = u.id
ORDER BY
    q.id,
    answer_id;

INSERT INTO users (id, username, password, role, created_at) VALUES ('adminid', 'admin', 'admin', 'ADMIN', DEFAULT);