package com.ecloud.api;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class ApiApplicationTests {

  @Test
  void contextLoads() {
  }

  @Configuration
  static class Config {

    @Bean
    public RedisConnectionFactory connectionFactory() {
      RedisConnectionFactory factory = Mockito.mock(RedisConnectionFactory.class);
      RedisConnection connection = Mockito.mock(RedisConnection.class);
      Mockito.when(factory.getConnection()).thenReturn(connection);
      return factory;
    }

  }


}
