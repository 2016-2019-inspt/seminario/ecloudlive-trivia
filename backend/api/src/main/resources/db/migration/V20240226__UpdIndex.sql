drop index user_answer_user_id_answer_id_key;

create unique index user_answer_user_id_answer_id_key
    on user_answer (user_id, answer_id, trivia_run_id);

drop index user_answer_user_id_question_id_key;

create unique index user_answer_user_id_question_id_key
    on user_answer (user_id, question_id, trivia_run_id);

create or replace view trivia_results_aggregate
            (users_count, question_count, correct_answers, incorrect_answers, unanswered_questions) as
SELECT
    COUNT(DISTINCT ua.user_id) AS users_count,
    COUNT(DISTINCT q.id) AS question_count,
    SUM(CASE WHEN a.correct = true THEN  1 ELSE  0 END) AS correct_answers,
    SUM(CASE WHEN a.correct = false THEN  1 ELSE  0 END) AS incorrect_answers,
    (COUNT(DISTINCT q.id) * COUNT(DISTINCT ua.user_id)) - COUNT(ua.id) AS unanswered_questions,
    trivia_run_id
FROM
    user_answer ua
        JOIN answers a ON ua.answer_id = a.id
        JOIN questions q ON a.question_id = q.id
GROUP BY
    trivia_run_id;