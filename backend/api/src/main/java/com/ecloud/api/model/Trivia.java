package com.ecloud.api.model;


import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@Table("trivia")
public class Trivia {

    @Id
    private String id;
    private String name;
    private String description;
    private Integer countdown;
    private String nextTriviaId;
    private Boolean shuffleAnswers;
    private LocalDateTime updatedAt;

}
