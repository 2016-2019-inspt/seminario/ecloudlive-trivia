package com.ecloud.api.controller;

import com.ecloud.api.model.Trivia;
import com.ecloud.api.model.TriviaProjection;
import com.ecloud.api.model.TriviaRunState;
import com.ecloud.api.model.TriviaWithQuestionCount;
import com.ecloud.api.service.TriviaService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trivia")
@RequiredArgsConstructor
@Slf4j
public class TriviaController {

  private final TriviaService triviaService;


  @GetMapping("/{id}")
  public ResponseEntity<TriviaWithQuestionCount> getTriviaById(@PathVariable String id) {
    TriviaWithQuestionCount trivia = triviaService.getTriviaById(id);
    log.debug("GET Trivia result: {}", trivia);
    return ResponseEntity.ok(trivia);
  }

  @GetMapping
  public ResponseEntity<List<TriviaWithQuestionCount>> getTriviaList() {
    List<TriviaWithQuestionCount> triviaList = triviaService.listTrivias();
    return ResponseEntity.ok(triviaList);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteTriviaById(@PathVariable String id) {
    triviaService.deleteTrivia(id);
    return ResponseEntity.ok().build();
  }

  @PostMapping
  public ResponseEntity<Trivia> upsertTrivia(@RequestBody Trivia trivia) {
    log.info("UPSERT TRIVIA with: {}", trivia);
    return ResponseEntity.ok(triviaService.updateOrCreateTrivia(trivia));
  }

  @PutMapping
  public ResponseEntity<Trivia> linkTrivias(@RequestParam("from") String fromTriviaId,
                                            @RequestParam("to") String toTriviaId) {
// TODO: 11/02/2024 validate not circular reference
    Trivia parentTrivia = triviaService.linkTrivias(fromTriviaId, toTriviaId);
    return ResponseEntity.ok(parentTrivia);
  }

}
