package com.ecloud.api.model;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@Table("users")
public class User {

//    @JsonIgnore
    @Id
    private String id;
    private String username;
    private String password;
    private UserRole role;
    private LocalDateTime createdAt;


}
