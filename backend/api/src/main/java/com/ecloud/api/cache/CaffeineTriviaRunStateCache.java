package com.ecloud.api.cache;

import com.ecloud.api.model.TriviaRunState;

// TODO: 26/01/2024 add configuration for dev env
// TODO: 26/01/2024 conditional bean creation when redis is disabled property
public class CaffeineTriviaRunStateCache implements TriviaRunStateCache {
    @Override
    public TriviaRunState get() {
        return null;
    }

    @Override
    public void set(TriviaRunState newState) {

    }

    @Override
    public void clear() {

    }
}
