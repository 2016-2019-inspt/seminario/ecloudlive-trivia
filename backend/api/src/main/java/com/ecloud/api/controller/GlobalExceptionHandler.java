package com.ecloud.api.controller;


import com.ecloud.api.exceptions.ErrorMessage;
import com.ecloud.api.exceptions.NotFoundException;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorMessage> notFoundRequest(NotFoundException ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        var errors = new ErrorMessage(
            LocalDateTime.now(),
            404,
            "Not Found",
            ex.getMessage(),
            request.getContextPath());
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> internalServerError(Exception ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        var errors = new ErrorMessage(
                LocalDateTime.now(),
                500,
                "Internal Server Error",
                ex.getMessage(),
                request.getContextPath());
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorMessage> badRequest(RuntimeException ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        var errors = new ErrorMessage(
                LocalDateTime.now(),
                400,
                "Bad Request",
                ex.getMessage(),
                request.getContextPath());
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

}
