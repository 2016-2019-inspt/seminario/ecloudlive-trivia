package com.ecloud.api.repository;

import com.ecloud.api.model.Question;
import java.util.List;
import java.util.Optional;
import lombok.NonNull;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.ListPagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Long>, ListPagingAndSortingRepository<Question, Long> {
  void deleteById(@NonNull Long id);

  @Modifying
  @Query("UPDATE questions SET \"order\" = :order WHERE id = :id")
  void updateOrder(@NonNull @Param("id") Long id, @NonNull @Param("order") Long order);


  @Query("SELECT id FROM questions WHERE trivia_id = :triviaId AND \"order\" = :order")
  Optional<Long> findIdByTriviaIdAndOrder(String triviaId, Integer order);

  List<Question> findAllByTriviaIdOrderByOrder(@NonNull @Param("triviaId") String triviaId);

/* Example for custom query
    @Query("SELECT c FROM Customer c WHERE c.email =:email")
    Customer findCustomerByEmail(String email);
 */
}
