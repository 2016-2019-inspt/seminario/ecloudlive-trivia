package com.ecloud.api.repository;

import com.ecloud.api.model.Answer;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends CrudRepository<Answer, Long> {

    List<Answer> findAllByQuestionId(long questionId);

}
