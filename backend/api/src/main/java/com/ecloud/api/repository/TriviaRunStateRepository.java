package com.ecloud.api.repository;

import com.ecloud.api.model.TriviaRunState;
import com.ecloud.api.model.TriviaRunStateEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TriviaRunStateRepository extends CrudRepository<TriviaRunState, String> {


    Optional<TriviaRunState> findFirstByStateInOrderByUpdatedAtDesc(List<TriviaRunStateEnum> states);

}
