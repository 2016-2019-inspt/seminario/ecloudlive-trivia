package com.ecloud.api.dto;

import com.ecloud.api.model.User;
import java.util.List;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class QuestionByUsersDto {
  Long questionId;
  String question;
  List<AnswerUsers> answers;


  @Builder
  @Value
  public static class AnswerUsers  {
    Long answerId;
    String answer;
    Boolean correct;
    List<User> users;
  }
}
