package com.ecloud.api.exceptions;


import java.time.LocalDateTime;

/**
 * @param status  Http status
 * @param error   Error title
 * @param message More detail for errors
 */

public record ErrorMessage(LocalDateTime timestamp, int status, String error, String message, String path) {
}
