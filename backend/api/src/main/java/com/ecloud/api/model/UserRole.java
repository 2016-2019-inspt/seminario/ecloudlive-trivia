package com.ecloud.api.model;

public enum UserRole {
    ADMIN, USER
}
