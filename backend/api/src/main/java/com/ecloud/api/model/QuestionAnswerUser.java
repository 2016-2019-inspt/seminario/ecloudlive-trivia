package com.ecloud.api.model;

import lombok.Value;

@Value
public class QuestionAnswerUser {
  Long questionId;
  String question;
  Long answerId;
  String answer;
  Boolean correct;
  String userId;
  String user;
}
