package com.ecloud.api.service;

import com.ecloud.api.dto.QuestionAggrUsersDto;
import com.ecloud.api.dto.QuestionAggrUsersDto.AnswerUsersCount;
import com.ecloud.api.dto.QuestionByUsersDto;
import com.ecloud.api.dto.QuestionByUsersDto.AnswerUsers;
import com.ecloud.api.dto.UserTriviaResultsDto;
import com.ecloud.api.model.QuestionAnswerAggrUser;
import com.ecloud.api.model.QuestionAnswerUser;
import com.ecloud.api.model.TriviaResultsAggregated;
import com.ecloud.api.model.User;
import com.ecloud.api.model.UserAnswerQuestion;
import com.ecloud.api.repository.ResultRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ResultsService {

  private final ResultRepository resultRepository;

  public TriviaResultsAggregated getTriviaResultsAggregated(String triviaRunId) {
    return resultRepository.getTriviaResultsAggregatedByTriviaRunId(triviaRunId);
  }

  public UserTriviaResultsDto listUserAnswerQuestionWithCorrect(String userId) {
    List<UserAnswerQuestion> userAnswerQuestions = resultRepository.listUserAnswerQuestion(userId);
    long passed = userAnswerQuestions.stream()
        .filter(u -> u.getUserAnswer().equals(u.getCorrect()))
        .count();
    return UserTriviaResultsDto.builder()
// TODO        .trivia()
        .answers(userAnswerQuestions)
        .passed(passed)
        .failed(userAnswerQuestions.size() - passed)
        .build();
  }

  public QuestionByUsersDto getQuestionResultsWithUsers(Long questionId) {
    List<QuestionAnswerUser> questionAnswerUsers = resultRepository.getQuestionResultsWithUsers(questionId);
    if (questionAnswerUsers.isEmpty()) {
      return null;
    }

    Map<Long, AnswerUsers> answerUsers = new HashMap<>();

    questionAnswerUsers.forEach(
        questionAnswerUser -> {
          var userList = answerUsers.computeIfAbsent(questionAnswerUser.getAnswerId(), ansId ->
              AnswerUsers.builder()
                  .answer(questionAnswerUser.getAnswer())
                  .answerId(questionAnswerUser.getAnswerId())
                  .users(new ArrayList<>())
                  .build()
          ).getUsers();
          if (questionAnswerUser.getUserId() != null) {
            userList.add(
                User.builder()
                    .id(questionAnswerUser.getUserId())
                    .username(questionAnswerUser.getUser())
                    .build());
          }
        });
    return QuestionByUsersDto.builder()
        .question(questionAnswerUsers.get(0).getQuestion())
        .questionId(questionId)
        .answers(new ArrayList<>(answerUsers.values()))
        .build();
  }

  public QuestionAggrUsersDto getQuestionResultsUsersCount(Long questionId) {
    List<QuestionAnswerAggrUser> questionAnswerAggrUsers = resultRepository.getQuestionResultsWithAggregatedUsers(questionId);
    if (questionAnswerAggrUsers.isEmpty()) {
      return null;
    }

    return QuestionAggrUsersDto.builder()
        .questionId(questionAnswerAggrUsers.get(0).getQuestionId())
        .question(questionAnswerAggrUsers.get(0).getQuestion())
        .answers(questionAnswerAggrUsers.stream()
            .map(questionAnswerAggrUser -> AnswerUsersCount.builder()
                .correct(questionAnswerAggrUser.getCorrect())
                .users(questionAnswerAggrUser.getUsers())
                .answerId(questionAnswerAggrUser.getAnswerId())
                .answer(questionAnswerAggrUser.getAnswer())
                .build()
            ).toList())
        .build();

  }

}
