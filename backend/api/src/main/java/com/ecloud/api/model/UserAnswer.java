package com.ecloud.api.model;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@Table("user_answer")
public class UserAnswer {

//    @JsonIgnore
    @Id
    private Long id;
    private String userId;
    private Long answerId;
    private Long questionId;
    private String triviaRunId;
    private LocalDateTime createdAt;

}
