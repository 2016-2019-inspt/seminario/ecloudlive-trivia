package com.ecloud.api.model;

public enum TriviaRunStateEnum {
  INITIAL,
  RUNNING,
  FINAL,
  ENDED
}
