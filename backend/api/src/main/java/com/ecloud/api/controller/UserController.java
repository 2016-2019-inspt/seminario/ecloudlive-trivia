package com.ecloud.api.controller;

import com.ecloud.api.exceptions.NotFoundException;
import com.ecloud.api.model.User;
import com.ecloud.api.model.UserAnswer;
import com.ecloud.api.model.UserRole;
import com.ecloud.api.repository.UserAnswerRepository;
import com.ecloud.api.repository.UserRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    @Value("${service.api-key}")
    String value;

    private final UserRepository userRepository;

    private final UserAnswerRepository userAnswerRepository;

    @GetMapping("/{username}")
    public ResponseEntity<User> getOrCreateUserByUsername(@PathVariable("username") String username,
        @RequestParam(value = "id", required=false) String id ) {

        User user;

        if (StringUtils.isNotBlank(id)) {
            user = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User with id, not found"));
        }
        else {
            user = userRepository.findByUsername(username)
                .orElseGet(() -> {
                    var genId = RandomStringUtils.randomAlphanumeric(12);
                    return userRepository.createByNameAndId(username, genId);
                });
        }

        return ResponseEntity.ok(user);
    }

    @PutMapping
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        var updated = userRepository.findById(user.getId())
            .map(found -> {
                if (user.getUsername() != null)
                    found.setUsername(user.getUsername());
                return userRepository.save(found);
            })
            .orElseThrow(() -> new NotFoundException("User with id, not found"));
        return ResponseEntity.ok(updated);
    }

    @PostMapping("/login")
    public ResponseEntity<User> loginByCredentials(@RequestBody UserCredentials credentials) {
        return userRepository.findByUsername(credentials.getUsername())
            .filter(user -> user.getPassword().equals(credentials.getPassword()) && user.getRole().equals(UserRole.ADMIN))
            .map(ResponseEntity::ok)
            .orElseThrow(() -> new NotFoundException("User not found with provided credentials..."));
    }

    @PostMapping("/question")
    public ResponseEntity<?> saveUserAnswer(@RequestBody UserAnswer userAnswer) {
        //TODO: Implementar buffer para guardar

        return ResponseEntity.ok(userAnswerRepository.save(userAnswer));
    }

    @Data
    public static class UserCredentials {
        private String username;
        private String password;
    }
}
