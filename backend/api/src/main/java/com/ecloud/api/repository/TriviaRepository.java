package com.ecloud.api.repository;

import com.ecloud.api.model.Trivia;
import com.ecloud.api.model.TriviaProjection;
import com.ecloud.api.model.TriviaWithQuestionCount;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface TriviaRepository extends CrudRepository<Trivia, String> {

    String FIND_ALL_WITH_QUESTION_COUNT_QUERY = """
        SELECT t.id, t.name, t.description, t.countdown, t.next_trivia_id, t.shuffle_answers,
        t.updated_at, COUNT(q.id) as question_count
        FROM trivia t
        LEFT JOIN questions q ON t.id = q.trivia_id
        GROUP BY t.id ORDER BY t.name ASC
        """;
    String FIND_BY_ID_PROJECTION_QUERY = """
        SELECT t.id, t.name, t.description, t.countdown, t.next_trivia_id, t.shuffle_answers, t.updated_at
        FROM trivia t
        WHERE t.id = :id
        """;

    String FIND_BY_ID_WITH_QUESTION_COUNT = """
        SELECT t.id, t.name, t.description, t.countdown, t.next_trivia_id, t.shuffle_answers, t.updated_at,
        COUNT(q.id) as question_count
        FROM trivia t
        LEFT JOIN questions q ON t.id = q.trivia_id
        WHERE t.id = :id
        GROUP BY t.id
        """;

    @Query(FIND_ALL_WITH_QUESTION_COUNT_QUERY)
    List<TriviaWithQuestionCount> findAllWithQuestionCount();

    @Query(FIND_BY_ID_PROJECTION_QUERY)
    Optional<TriviaWithQuestionCount> findByIdProjection(@Param("id") String id);

    @Query(FIND_BY_ID_WITH_QUESTION_COUNT)
    TriviaWithQuestionCount findByIdWithQuestionCount(@Param("id") String id);

}
