package com.ecloud.api.cache;

import com.ecloud.api.model.TriviaRunState;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
@RequiredArgsConstructor
@ConditionalOnProperty("trivia.redis.enabled")
public class RedisTriviaRunStateCache implements TriviaRunStateCache {

    private static final String TRIVIA_RUN_CACHE_KEY = "run_status";
    private static final Duration TRIVIA_RUN_CACHE_TTL = Duration.ofMinutes(15);
    private final RedisTemplate<String, TriviaRunState> triviaRunStateRedisTemplate;

    @Override
    public TriviaRunState get() {
        return triviaRunStateRedisTemplate.opsForValue().get(TRIVIA_RUN_CACHE_KEY);
    }

    @Override
    public void set(TriviaRunState newState) {
        triviaRunStateRedisTemplate.opsForValue().set(TRIVIA_RUN_CACHE_KEY, newState, TRIVIA_RUN_CACHE_TTL);
    }

    @Override
    public void clear() {
        triviaRunStateRedisTemplate.opsForValue().getAndDelete(TRIVIA_RUN_CACHE_KEY);
    }
}
