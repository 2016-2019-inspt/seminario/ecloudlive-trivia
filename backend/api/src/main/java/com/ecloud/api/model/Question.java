package com.ecloud.api.model;

import java.util.Set;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@Table("questions")
public class Question {

    @Id
    private Long id;
    private String text;
    private Integer order;
    private String triviaId;
    @MappedCollection(idColumn = "question_id")
    private Set<Answer> answers;

}
