package com.ecloud.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@NoArgsConstructor
@Getter
@Table("trivia_run_state")
public class TriviaRunState {

    @Id
    private String id;
    private String triviaId;
    private String nextTriviaId;
    private TriviaRunStateEnum state;
    private Long currentQuestionId;
    private Integer currentOrder;
    private Boolean showAnswer;
    private Boolean timeout;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDateTime createdAt;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDateTime updatedAt;

    public TriviaRunState setId(String id) {
        this.id = id;
        return this;
    }

    public TriviaRunState setState(TriviaRunStateEnum state) {
        this.state = state;
        return this;
    }

    public TriviaRunState setTriviaId(String triviaId) {
        this.triviaId = triviaId;
        return this;
    }

    public TriviaRunState setNextTriviaId(String nextTriviaId) {
        this.nextTriviaId = nextTriviaId;
        return this;
    }

    public TriviaRunState setCurrentOrder(Integer currentOrder) {
        this.currentOrder = currentOrder;
        return this;
    }

    public TriviaRunState setShowAnswer(Boolean showAnswer) {
        this.showAnswer = showAnswer;
        return this;
    }

    public TriviaRunState setTimeout(Boolean timeout) {
        this.timeout = timeout;
        return this;
    }

    public TriviaRunState setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public TriviaRunState setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public TriviaRunState setCurrentQuestionId(Long currentQuestionId) {
        this.currentQuestionId = currentQuestionId;
        return this;
    }
}
