package com.ecloud.api.cache;

import com.ecloud.api.model.TriviaRunState;

public interface TriviaRunStateCache {

    TriviaRunState get();
    void set(TriviaRunState newState);
    void clear();
}
