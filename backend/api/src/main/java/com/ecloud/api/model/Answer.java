package com.ecloud.api.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@Table("answers")
public class Answer {

    @Id
    private Long id;
    private String text;
    private Boolean correct;
    private Long questionId;
    private Integer order;

}
