package com.ecloud.api.repository;

import com.ecloud.api.model.User;
import java.util.Optional;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    Optional<User> findByUsername(@NonNull String username);

    @Query("INSERT INTO users (username, id) VALUES (:username, :id) RETURNING *")
    User createByNameAndId(@NonNull @Param("username") String username, @NonNull @Param("id") String id);

}
