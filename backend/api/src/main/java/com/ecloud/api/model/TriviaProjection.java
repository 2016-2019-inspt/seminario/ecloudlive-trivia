package com.ecloud.api.model;

import java.time.LocalDateTime;

public interface TriviaProjection {

    String getId();
    String getName();
    String getDescription();
    Integer getCountdown();
    String getNextTriviaId();
    Boolean getShuffleAnswers();
    LocalDateTime getUpdatedAt();

}