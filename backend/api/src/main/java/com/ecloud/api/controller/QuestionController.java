package com.ecloud.api.controller;

import com.ecloud.api.model.Question;
import com.ecloud.api.repository.AnswerRepository;
import com.ecloud.api.repository.QuestionRepository;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/questions")
@RequiredArgsConstructor
@Slf4j
public class QuestionController {

    private final QuestionRepository questionRepository;

    @GetMapping
    public List<Question> getQuestions(@RequestParam("triviaId") String triviaId) {
        return questionRepository.findAllByTriviaIdOrderByOrder(triviaId);
    }

    @PostMapping
    public Question createQuestion(@RequestBody Question question) {
        log.debug("Question to Save: {}", question);
        return questionRepository.save(question);
    }

    @PutMapping
    public ResponseEntity<Question> updateQuestion(@RequestBody Question question) {
        if (question.getId() != null) {
            return ResponseEntity.ok(questionRepository.save(question));
        }
        log.error("Required field id missing: {}", question);
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/{id}")
    public Question getQuestionById(@PathVariable("id") Long id) {
        return questionRepository.findById(id)
                .orElse(Question.builder().build());
    }

    @DeleteMapping("/{id}")
    public void deleteQuestionById(@PathVariable("id") Long id) {
        questionRepository.deleteById(id);
    }

    @PatchMapping("/order")
    public void updateQuestionOrder(@RequestBody List<QuestionOrder> questionOrders) {
        questionOrders.forEach(questionOrder -> questionRepository.updateOrder(questionOrder.id, questionOrder.order));
    }


    @Data
    public static class QuestionOrder {
        private Long id;
        private Long order;
    }
}
