package com.ecloud.api.service;

import com.ecloud.api.cache.TriviaRunStateCache;
import com.ecloud.api.exceptions.NotFoundException;
import com.ecloud.api.model.Trivia;
import com.ecloud.api.model.TriviaProjection;
import com.ecloud.api.model.TriviaRunState;
import com.ecloud.api.model.TriviaRunStateEnum;
import com.ecloud.api.model.TriviaWithQuestionCount;
import com.ecloud.api.repository.QuestionRepository;
import com.ecloud.api.repository.TriviaRepository;
import com.ecloud.api.repository.TriviaRunStateRepository;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jdbc.core.JdbcAggregateTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TriviaService {

  // Trivia API
  private final TriviaRepository triviaRepository;
  private final JdbcAggregateTemplate jdbcAggregateTemplate;

  public TriviaWithQuestionCount getTriviaById(String id) {
    return triviaRepository.findByIdWithQuestionCount(id);
  }

  public List<TriviaWithQuestionCount> listTrivias() {
//    return StreamSupport.stream(triviaRepository.findAll().spliterator(), false)
//        .toList();
    return triviaRepository.findAllWithQuestionCount();
  }

  public Trivia updateOrCreateTrivia(Trivia trivia) {
    if (StringUtils.isNotBlank(trivia.getId())){
      var result = triviaRepository.save(trivia);
      log.debug("Trivia updated: {}", result);
      return result;
    }
    else {
      trivia.setId(UUID.randomUUID().toString());
      var result = jdbcAggregateTemplate.insert(trivia);
      log.debug("Trivia created: {}", result);
      return result;
    }
  }

  public void deleteTrivia(String id) {
    triviaRepository.deleteById(id);
  }

  // todo: create a new table of 1:1 relation, extract next trivia column from trivia table
  public Trivia linkTrivias(String fromTriviaId, String toTriviaId) {
    Trivia parent = triviaRepository.findById(toTriviaId)
        .orElseThrow();
    Trivia child = triviaRepository.findById(fromTriviaId)
        .orElseThrow();
    parent.setNextTriviaId(fromTriviaId);

    return triviaRepository.save(parent);
  }



}
