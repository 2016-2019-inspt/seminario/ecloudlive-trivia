package com.ecloud.api.controller;

import com.ecloud.api.model.TriviaRunState;
import com.ecloud.api.service.TriviaRunStateService;
import com.ecloud.api.service.TriviaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trivia/run")
@RequiredArgsConstructor
@Slf4j
public class TriviaRunController {

    private final TriviaRunStateService triviaRunStateService;

    // TODO: 26/01/2024 list runnable trivias. filter by draft, ready, etc. statuses
    // TODO: 28/01/2024 each time a trivia starts, create a record, user responses will use that id,
    // TODO: 28/01/2024 this way, getting the results will not aggregate older values

    @GetMapping
    public TriviaRunState getTriviaRun() {
        return triviaRunStateService.getTriviaRunState();
    }

    // start, reset and next trivia with proper id:
    @PostMapping("/start/{triviaId}")
    public ResponseEntity<TriviaRunState> startTrivia(@PathVariable("triviaId") String triviaId) {
        TriviaRunState initialTriviaState = triviaRunStateService.createTriviaRunState(triviaId);
        return ResponseEntity.ok(initialTriviaState);
    }

    @PutMapping("/next_question")
    public ResponseEntity<TriviaRunState> nextQuestion() {
        TriviaRunState nextQuestionState = triviaRunStateService.nextQuestion();
        log.debug("Next State: {}", nextQuestionState);
        return ResponseEntity.ok(nextQuestionState);
    }

    @DeleteMapping("/end")
    public ResponseEntity<TriviaRunState> endTrivia() {
        return ResponseEntity.ok(triviaRunStateService.endTriviaRunState());
    }

    @PostMapping("/timeout")
    public ResponseEntity<TriviaRunState> triviaTimeout() {
        return ResponseEntity.ok(triviaRunStateService.timeoutTrivia());
    }

    @PostMapping("/show_correct")
    public ResponseEntity<TriviaRunState> triviaShowCorrect() {
        return ResponseEntity.ok(triviaRunStateService.showCorrectTrivia());
    }

    @PatchMapping
    public  ResponseEntity<TriviaRunState> upsertTriviaRun(@RequestBody TriviaRunState run) {
        log.debug("Incoming Trivia Run Status to Update: {}", run);
        TriviaRunState updatedStatus = triviaRunStateService.updateTriviaRun(run);
        log.debug("Actual updated trivia Run Status: {}", updatedStatus);
        return ResponseEntity.ok(updatedStatus);
    }
}
