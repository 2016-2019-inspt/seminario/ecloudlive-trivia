package com.ecloud.api.controller;

import com.ecloud.api.dto.QuestionAggrUsersDto;
import com.ecloud.api.dto.QuestionByUsersDto;
import com.ecloud.api.dto.UserTriviaResultsDto;
import com.ecloud.api.model.TriviaResultsAggregated;
import com.ecloud.api.service.ResultsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/results")
@RequiredArgsConstructor
@Slf4j
public class ResultController {

  private final ResultsService resultsService;

  @GetMapping("/final/{triviaRunId}")
  public ResponseEntity<TriviaResultsAggregated> getTriviaFinalResults(@PathVariable String triviaRunId) {
    TriviaResultsAggregated aggregated = resultsService.getTriviaResultsAggregated(triviaRunId);
    return ResponseEntity.ok(aggregated);
  }

  @GetMapping("/user/{userId}")
  public ResponseEntity<UserTriviaResultsDto> getTriviaResultsByUser(@PathVariable String userId) {
    UserTriviaResultsDto resultsDto = resultsService.listUserAnswerQuestionWithCorrect(userId);
    return ResponseEntity.ok(resultsDto);
  }

  @GetMapping("/question/{questionId}")
  public ResponseEntity<QuestionByUsersDto> getQuestionResultsByUsersInAnswers(@PathVariable Long questionId) {
    QuestionByUsersDto questionByUsersDto = resultsService.getQuestionResultsWithUsers(questionId);
    return ResponseEntity.ok(questionByUsersDto);
  }

  @GetMapping("/question/{questionId}/count_users")
  public ResponseEntity<QuestionAggrUsersDto> getQuestionResultsAggregatedUsersInAnswers(@PathVariable Long questionId) {
    QuestionAggrUsersDto questionAnswerAggrUser = resultsService.getQuestionResultsUsersCount(questionId);
    return ResponseEntity.ok(questionAnswerAggrUser);
  }
}
