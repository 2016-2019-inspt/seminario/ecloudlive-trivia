package com.ecloud.api.model;

import lombok.Value;

@Value
public class QuestionAnswerAggrUser {
  Long questionId;
  String question;
  Long answerId;
  String answer;
  Boolean correct;
  Long users;
}
