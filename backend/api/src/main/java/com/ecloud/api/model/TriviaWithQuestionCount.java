package com.ecloud.api.model;

import lombok.EqualsAndHashCode;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Table("trivia")
public class TriviaWithQuestionCount extends Trivia {
    private Integer questionCount;

    public Integer getQuestionCount() {
        return questionCount;
    }

    public TriviaWithQuestionCount setQuestionCount(Integer questionCount) {
        this.questionCount = questionCount;
        return this;
    }

    TriviaWithQuestionCount(String id, String name, String description, Integer countdown,
                            String nextTriviaId, Boolean shuffleAnswers, LocalDateTime updatedAt, Integer questionCount) {
        super(id, name, description, countdown, nextTriviaId, shuffleAnswers, updatedAt);
        this.questionCount = questionCount;
    }
}
