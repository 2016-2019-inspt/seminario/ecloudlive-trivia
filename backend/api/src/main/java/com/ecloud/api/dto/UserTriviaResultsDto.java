package com.ecloud.api.dto;

import com.ecloud.api.model.UserAnswerQuestion;
import java.util.List;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class UserTriviaResultsDto {
  String trivia;
  Long passed;
  Long failed;
  List<UserAnswerQuestion> answers;

}
