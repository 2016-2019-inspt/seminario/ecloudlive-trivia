package com.ecloud.api.exceptions;

public class ValidationException extends RuntimeException {
  public ValidationException(String message) {
    super(message);
  }

  public ValidationException(String message, Object... args) {
    super(message.formatted(args));
  }
}
