package com.ecloud.api.model;

import lombok.Value;

@Value
public class UserAnswerQuestion {
  String question;
  String userAnswer;
  String correct;
}
