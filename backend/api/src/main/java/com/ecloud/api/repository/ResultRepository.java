package com.ecloud.api.repository;

import com.ecloud.api.model.QuestionAnswerAggrUser;
import com.ecloud.api.model.QuestionAnswerUser;
import com.ecloud.api.model.TriviaResultsAggregated;
import com.ecloud.api.model.UserAnswerQuestion;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ResultRepository {

  private static final String RESULTS_AGGREGATE_QUERY = "SELECT * FROM trivia_results_aggregate";

  private static final String RESULTS_AGGREGATE_QUERY_BY_ID = "SELECT * FROM trivia_results_aggregate WHERE trivia_run_id = ?";

  private static final String USER_ANSWER_QUESTION_QUERY = "SELECT * FROM user_answers_question WHERE user_id = ?";

  private static final String QUESTION_RESULTS_QUERY = "SELECT * FROM question_results WHERE question_id = ?";

  private static final String QUESTION_RESULTS_AGGREGATED_QUERY = """
      SELECT
          question_id,
          question,
          answer_id,
          answer,
          correct,
          COUNT(user_id) AS users
      FROM question_results
      WHERE question_id = ?
      GROUP BY question_id, question, answer_id, answer, correct
    """;


  private final JdbcTemplate jdbcTemplate;

//  public TriviaResultsAggregated getTriviaResultsAggregated() {
//    return jdbcTemplate.query(RESULTS_AGGREGATE_QUERY,
//            (rs, rowNum) -> TriviaResultsAggregated.builder()
//                .users(rs.getLong("users_count"))
//                .correct(rs.getLong("correct_answers"))
//                .incorrect(rs.getLong("incorrect_answers"))
//                .notAnswered(rs.getLong("unanswered_questions"))
//                .build()
//        ).stream().findFirst()
//        .orElse(null);
//  }

  // FIXME: 28/02/2024 counts = 0 when no answer provided by any user
  public TriviaResultsAggregated getTriviaResultsAggregatedByTriviaRunId(String triviaRunId) {
    return jdbcTemplate.query(RESULTS_AGGREGATE_QUERY_BY_ID,
            (rs, rowNum) -> TriviaResultsAggregated.builder()
                .users(rs.getLong("users_count"))
                .correct(rs.getLong("correct_answers"))
                .incorrect(rs.getLong("incorrect_answers"))
                .notAnswered(rs.getLong("unanswered_questions"))
                .build(), triviaRunId)
        .stream()
        .findFirst()
        .orElse(TriviaResultsAggregated.builder()
            .users(0L)
            .notAnswered(0L)
            .correct(0L)
            .incorrect(0L)
            .build());
  }

  public List<UserAnswerQuestion> listUserAnswerQuestion(String userId) {
    return jdbcTemplate.query(USER_ANSWER_QUESTION_QUERY,
        (rs, rowNum) -> new UserAnswerQuestion(
            rs.getString("question"),
            rs.getString("answer"),
            rs.getString("correct")
        ),
        userId);
  }

  // TODO: 30/7/23 Add limit and paging
  public List<QuestionAnswerUser> getQuestionResultsWithUsers(Long questionId) {
    return jdbcTemplate.query(QUESTION_RESULTS_QUERY,
        (rs, rowNum) -> new QuestionAnswerUser(
            questionId,
            rs.getString("question"),
            rs.getLong("answer_id"),
            rs.getString("answer"),
            rs.getBoolean("correct"),
            rs.getString("user_id"),
            rs.getString("user")
        ),
        questionId);
  }

  public List<QuestionAnswerAggrUser> getQuestionResultsWithAggregatedUsers(Long questionId) {
    return jdbcTemplate.query(QUESTION_RESULTS_AGGREGATED_QUERY,
        (rs, rowNum) -> new QuestionAnswerAggrUser(
            questionId,
            rs.getString("question"),
            rs.getLong("answer_id"),
            rs.getString("answer"),
            rs.getBoolean("correct"),
            rs.getLong("users")
        ),
        questionId);
  }

}
