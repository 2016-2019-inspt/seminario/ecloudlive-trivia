package com.ecloud.api.dto;

import java.util.List;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class QuestionAggrUsersDto {
  Long questionId;
  String question;
  List<AnswerUsersCount> answers;


  @Builder
  @Value
  public static class AnswerUsersCount  {
    Long answerId;
    String answer;
    Boolean correct;
    Long users;
  }
}
