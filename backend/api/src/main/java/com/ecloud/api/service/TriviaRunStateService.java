package com.ecloud.api.service;

import com.ecloud.api.cache.TriviaRunStateCache;
import com.ecloud.api.exceptions.NotFoundException;
import com.ecloud.api.model.TriviaRunState;
import com.ecloud.api.model.TriviaRunStateEnum;
import com.ecloud.api.repository.QuestionRepository;
import com.ecloud.api.repository.TriviaRepository;
import com.ecloud.api.repository.TriviaRunStateRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jdbc.core.JdbcAggregateTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class TriviaRunStateService {

    private static final String TRIVIA_RUN_ID = "trivia_run_stage";

    private final JdbcAggregateTemplate jdbcAggregateTemplate;
    private final TriviaRunStateRepository triviaRunStateRepository;
    private final QuestionRepository questionRepository;
    private final TriviaRepository triviaRepository;
    private final TriviaRunStateCache triviaRunStateCache;


    public TriviaRunState getTriviaRunState() {
        var cachedStatus = triviaRunStateCache.get();

        if(cachedStatus != null) {
            log.debug("Returning status from cache: {}", cachedStatus);
            return cachedStatus;
        }
        else {
            TriviaRunState newStatus = triviaRunStateRepository
                .findFirstByStateInOrderByUpdatedAtDesc(
                    List.of(TriviaRunStateEnum.INITIAL, TriviaRunStateEnum.RUNNING, TriviaRunStateEnum.FINAL))
//                .orElseThrow(() -> new NotFoundException("No trivia running yet, call /run/start first"));
                .orElse(null);
            if (newStatus != null) {
                log.debug("Current Trivia Status: {}", newStatus);
                triviaRunStateCache.set(newStatus);
            }
            log.info("retunring state: {}", newStatus);
            return newStatus;
        }
    }

    // Also next trivia w/ nextTriviaId
    // Also reset trivia w/ same id
    public TriviaRunState createTriviaRunState(String triviaId) {

        var currentTrivia = getTriviaRunState();
        if(currentTrivia != null && currentTrivia.getTriviaId().equals(triviaId)) {
            return currentTrivia;
        }

        var trivia = Optional.ofNullable(triviaRepository.findByIdWithQuestionCount(triviaId))
            .orElseThrow(() -> new NotFoundException("Cannot start trivia with id: %s"+triviaId+", Trivia not found"));
        var nextQuestionId = questionRepository.findIdByTriviaIdAndOrder(triviaId, 0)
            .orElseThrow(() -> new NotFoundException("Cannot start trivia with id: %s"+triviaId+", questions not found"));

        var newState = new TriviaRunState()
            .setId(UUID.randomUUID().toString())
            .setState(TriviaRunStateEnum.INITIAL)
            .setTriviaId(trivia.getId())
            .setNextTriviaId(trivia.getNextTriviaId())
            .setCurrentQuestionId(nextQuestionId)
            .setCurrentOrder(-1)
            .setShowAnswer(false)
            .setTimeout(false)
            .setCreatedAt(LocalDateTime.now())
            .setUpdatedAt(LocalDateTime.now());
        triviaRunStateCache.set(newState);
        try {
            return jdbcAggregateTemplate.insert(newState);
        } catch (Exception de) {
            return triviaRunStateRepository.save(newState);
        }
    }

    public TriviaRunState endTriviaRunState() {
        var currentState = getTriviaRunState();
        currentState.setState(TriviaRunStateEnum.ENDED);
        triviaRunStateRepository.save(currentState);
        triviaRunStateCache.clear();
        return currentState;
    }

    public TriviaRunState nextQuestion() {
        var currentState = getTriviaRunState();

        if (currentState.getState().equals(TriviaRunStateEnum.FINAL)) {
            if(currentState.getNextTriviaId()==null) {
                return endTriviaRunState();
            }
            return createTriviaRunState(currentState.getNextTriviaId());
        }

        int nextOrder = currentState.getCurrentOrder() + 1;
        var nextQuestionId = questionRepository
            .findIdByTriviaIdAndOrder(currentState.getTriviaId(), nextOrder);

        var newRunState = nextQuestionId
            .map(qId -> currentState.setState(TriviaRunStateEnum.RUNNING)
                .setCurrentOrder(nextOrder)
                .setCurrentQuestionId(qId)
                .setShowAnswer(false)
                .setTimeout(false))
            .orElseGet(()-> currentState.setState(TriviaRunStateEnum.FINAL)
                .setCurrentQuestionId(null)
                .setCurrentOrder(-1)
            );
        triviaRunStateRepository.save(newRunState);
        triviaRunStateCache.set(newRunState);
        return newRunState;
    }

    public TriviaRunState updateTriviaRun(TriviaRunState triviaRunState) {
        TriviaRunState dbStatus = triviaRunStateRepository.findById(TRIVIA_RUN_ID)
            .map(oldObj -> nonNullDifference(triviaRunState, oldObj))
            .map(triviaRunStateRepository::save)
            .orElseGet(() -> {
                triviaRunState.setTimeout(false);
                triviaRunState.setShowAnswer(false);
                triviaRunState.setUpdatedAt(LocalDateTime.now());
                return triviaRunStateRepository.save(triviaRunState);
            });
        triviaRunStateCache.set(dbStatus);
        return dbStatus;
    }

    public TriviaRunState timeoutTrivia() {
        var currentTrivia = getTriviaRunState();
        if (currentTrivia != null) {
            currentTrivia.setTimeout(true);
            triviaRunStateRepository.save(currentTrivia);
            triviaRunStateCache.set(currentTrivia);
            return currentTrivia;
        }
        else {
            return null;
        }
    }

    public TriviaRunState showCorrectTrivia() {
        var currentTrivia = getTriviaRunState();
        if (currentTrivia != null) {
            currentTrivia.setShowAnswer(true);
            triviaRunStateRepository.save(currentTrivia);
            triviaRunStateCache.set(currentTrivia);
            return currentTrivia;
        }
        else {
            return null;
        }
    }


    private TriviaRunState nonNullDifference(TriviaRunState newObj, TriviaRunState oldObj) {
        log.debug("New Status: {} to update Older: {}", newObj, oldObj);
        return oldObj
            .setState(newObj.getState() != null ? newObj.getState() : oldObj.getState())
            .setShowAnswer(newObj.getShowAnswer() != null ? newObj.getShowAnswer() : oldObj.getShowAnswer())
            .setTimeout(newObj.getTimeout() != null ? newObj.getTimeout() : oldObj.getTimeout())
            .setUpdatedAt(LocalDateTime.now());
    }
}
