package com.ecloud.api.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TriviaResultsAggregated {
  private Long users;
  private Long correct;
  private Long incorrect;
  private Long notAnswered;
}