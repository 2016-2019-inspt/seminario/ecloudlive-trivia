#!/bin/sh


run_be_dev() {
  echo "Starting backend development required containers: db, redis, pgadmin & redisinsight"
  docker compose -f docker-compose-dev.yml -p ecloud-trivia-dev up -d \
  db redis
}

run_fe_dev() {
  echo "Starting frontend development required containers: db, redis, pgadmin, redisinsight & api"
  docker compose -f docker-compose-dev.yml -p ecloud-trivia-dev up -d \
  db pgadmin redis redisinsight api
}

run_dev() {
  echo "Starting all development containers for integration end-to-end testing..."
  docker compose -f docker-compose-dev.yml -p ecloud-trivia-dev up -d
}

run_be_prod() {
  echo "Starting backend production required containers: db, redis, 2 api instances & api_lb"
  docker compose -f docker-compose.yml -p ecloud-trivia up -d \
  db redis api api_lb --scale api=2
}

run_prod() {
  echo "Starting production deployment"
  docker compose -f docker-compose.yml -p ecloud-trivia up -d \
  --scale api=3 --scale app=2
}

option=$1
case $option in
    "be-dev")
        echo "db and network. call localhost and exposed port"
        run_be_dev
        ;;
    "fe-dev")
        echo "run db, api and network. call localhost"
        run_fe_dev
        ;;
    "dev")
        echo "run all app in dev mode no lb or scaling"
        run_dev
        ;;
    "be-prod")
        echo "run api behind the lb, scaling x2"
        run_be_prod
        ;;
    "prod")
        echo "run all be and fe behind the lb, scaling x3"
        run_prod
        ;;
    *)
        echo "Unknown option. Please use one of the following: "
        echo "be-dev        starts db, and db manager for spring backend local api development"
        echo "fe-dev        starts db, db manager and api for local frontend development"
        echo "dev           starts minimal containers for local end to end testing"
        echo "be-prod       starts db, api and lb for local integration testing
                with local frontend in dev mode"
        echo "prod          starts all db, lb, api and webapp in prod mode"
        ;;
esac