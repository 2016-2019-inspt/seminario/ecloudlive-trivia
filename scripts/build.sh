#!/bin/sh

# switch option show options if not provided
option=$1


build_api() {
  cd ./backend/api || exit
  ./gradlew clean build
  cd ../..
}

build_webapp() {
  cd ./webapp/ || exit
  yarn
  yarn build
  cd ..
}

# build api image and tag: 1. easy local development 2. gitlab registry
build_api_image() {
  docker build ./backend/api -t trivia_api -t registry.gitlab.com/freedomtree/customers/ecloudlive-trivia:api-1.0.0
}

# build webapp image and tag 1. easy local development 2. gitlab registry
build_webapp_image() {
  docker build ./webapp -t trivia_app -t registry.gitlab.com/freedomtree/customers/ecloudlive-trivia:app-1.0.0
}

case $option in
    "api")
        echo "Building Springboot application..."
        build_api
        ;;
    "webapp")
        echo "Building Next application..."
        build_webapp
        ;;
    "api-img")
        echo "Building Springboot docker image..."
        build_api_image
        ;;
    "webapp-img")
        echo "Building Next docker image..."
        build_webapp_image
        ;;
    "all")
        echo "Building all for production ready..."
        build_api
        build_api_image
        build_webapp
        build_webapp_image
        if [ $? -ne 0 ]; then
            echo "Error occurred."
        fi
        echo "Api and WebApp images are ready to push into the registry!"
        ;;
    *)
        echo "Unknown option. Please use one of the following: "
        echo "api           Test and build just the springboot application"
        echo "webapp        Try to build the prod ready version for the Next app"
        echo "api-img       Build the docker image with 2 tags. one ready for local dev
              and a second ready for the gitlab registry"
        echo "webapp-img    Same as above but for the webapp"
        echo "all           Runs all 4 commands in order, exits if error occurred"
        ;;
esac